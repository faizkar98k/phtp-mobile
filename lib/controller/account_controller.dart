import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/user.dart';
import 'package:phtp_mobile/data/repository/account_repository.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/view/home.dart';

class AccountController extends GetxController {
  AccountRepository accountRepository = AccountRepository();

  final emailController = TextEditingController().obs;
  final passwordController = TextEditingController().obs;
  final isUnAuthorised = false.obs;
  final loginLoading = false.obs;

  final user = User().obs;

  login(context) async {
    loginLoading(true);

    User userData = await accountRepository.login({
      'email': emailController.value.text,
      'password': passwordController.value.text,
    });

    if (userData == null) {
      AppModal.showInformation(
        context,
        'Tidak Berjaya',
        'Emel dan kata laluan anda salah.',
      );
    } else {
      user(userData);
      AppRoute.pushAndRemoveUntil(context, Home());
    }

    loginLoading(false);
  }

  logout() async {
    await AppStorage.storage.erase();
    await accountRepository.logout();
  }

  changePassword(context, oldPassword, newPassword) async {
    String token = AppStorage.storage.read('access_token');
    bool flag = await accountRepository.changePassword(
      {
        'password': oldPassword,
        'confirm_password': newPassword,
      },
      token,
    );

    if (!flag) {
      AppModal.showInformation(
        context,
        'Tidak Berjaya',
        'Kata laluan lama anda tidak betul.',
      );
    } else {
      await AppModal.showInformation(
        context,
        'Berjaya',
        'Kata laluan anda telah dikemaskini.',
      );
      AppRoute.pop(context);
    }
  }

  updateProfile(name, address, phone, context) async {
    String token = AppStorage.storage.read('access_token');
    bool data = await accountRepository.updateProfile(
      {
        'name': name,
        'phone_no': phone,
        'address': address,
      },
      token,
    );

    if (data) {
      await AppModal.showInformation(
        context,
        'Berjaya',
        'Profil anda telah dikemaskini.',
      );

      AppRoute.pop(context);
    }

    await getUserProfile();
  }

  getUserProfile() async {
    String token = AppStorage.storage.read('access_token');

    User userData = await accountRepository.getUserProfile(token);

    if (userData != null) {
      user(userData);
    }
  }
}
