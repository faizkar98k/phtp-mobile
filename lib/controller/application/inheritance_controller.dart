import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/inheritance.dart';
import 'package:phtp_mobile/data/repository/inheritance_repository.dart';

class InheritanceController extends GetxController {
  InheritanceRepository inheritanceRepository = InheritanceRepository();

  final noLot = TextEditingController().obs;
  final noHakMilik = TextEditingController().obs;
  final alamat = TextEditingController().obs;

  final inheritanceList = List<Inheritance>().obs;

  createInheritance(file) async {
    String token = AppStorage.storage.read('access_token');
    int applicationId = AppStorage.storage.read('application_id');

    await inheritanceRepository.createInheritance(
      token,
      file,
      noLot.value.text,
      noHakMilik.value.text,
      alamat.value.text,
      applicationId.toString(),
    );

    noLot.value.text = '';
    noHakMilik.value.text = '';
    alamat.value.text = '';

    await getInheritance();
  }

  getInheritance() async {
    inheritanceList.clear();

    String token = AppStorage.storage?.read('access_token');
    int applicationId = AppStorage.storage?.read('application_id');

    final List<Inheritance> list = await inheritanceRepository.getInheritance(
      {'application_id': applicationId.toString()},
      token,
    );

    if (list.isNotEmpty) {
      inheritanceList.assignAll(list);
    } else {
      return null;
    }
  }

  deleteInheritance(id) async {
    String token = AppStorage.storage?.read('access_token');

    await inheritanceRepository.deleteInheritance({'id': id.toString()}, token);
    await getInheritance();
  }

  updateInheritance(file, id) async {
    String token = AppStorage.storage.read('access_token');

    await inheritanceRepository.updateInheritance(
      token,
      file,
      noLot.value.text,
      noHakMilik.value.text,
      alamat.value.text,
      id.toString(),
    );

    noLot.value.text = '';
    noHakMilik.value.text = '';
    alamat.value.text = '';

    await getInheritance();
  }
}
