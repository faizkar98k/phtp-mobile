import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/benificiary.dart';
import 'package:phtp_mobile/data/repository/application_repository.dart';

class BenificiaryController extends GetxController {
  ApplicationRepository applicationRepository = ApplicationRepository();
  ApplicationController applicationController =
      Get.put(ApplicationController());

  final name = TextEditingController().obs;
  final kadPengenalan = TextEditingController().obs;
  final hubungan = 'Tiada Maklumat'.obs;
  final alamat = TextEditingController().obs;
  final telefon = TextEditingController().obs;

  final benificiaryList = List<Benificiary>().obs;

  createBenificiary(file) async {
    String token = AppStorage.storage?.read('access_token');
    int applicationId = AppStorage.storage?.read('application_id');

    await applicationRepository.createBenificiary(
      token,
      file,
      name.value.text,
      hubungan.value,
      kadPengenalan.value.text,
      alamat.value.text,
      telefon.value.text,
      applicationId.toString(),
    );
    await getBenificiary();

    name.value.text = '';
    kadPengenalan.value.text = '';
    hubungan.value = 'Tiada Maklumat';
    alamat.value.text = '';
    telefon.value.text = '';
  }

  getBenificiary() async {
    benificiaryList.clear();

    String token = AppStorage.storage?.read('access_token');
    int applicationId = AppStorage.storage?.read('application_id');

    print(applicationId);

    final List<Benificiary> list = await applicationRepository.getBenificiary(
      {'application_id': applicationId.toString()},
      token,
    );

    if (list.isNotEmpty) {
      benificiaryList.assignAll(list);
    } else {
      return null;
    }
  }

  deleteBenificiary(id) async {
    String token = AppStorage.storage?.read('access_token');

    await applicationRepository.deleteBenificiary({'id': id.toString()}, token);
    await getBenificiary();
  }

  updateBenificiary(file, id) async {
    String token = AppStorage.storage?.read('access_token');

    await applicationRepository.updateBenificiary(
      token,
      file,
      name.value.text,
      hubungan.value,
      kadPengenalan.value.text,
      alamat.value.text,
      telefon.value.text,
      id.toString(),
    );
    await getBenificiary();

    name.value.text = '';
    kadPengenalan.value.text = '';
    hubungan.value = 'Tiada Maklumat';
    alamat.value.text = '';
    telefon.value.text = '';
  }
}
