import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/data/repository/application_repository.dart';
import 'package:phtp_mobile/utils/app_constant.dart';

class ApplicationController extends GetxController {
  final applicationList = List<Application>().obs;
  final application = Application().obs;
  final loading = false.obs;
  final namaPemohon = TextEditingController().obs;
  final tarafKepentingan = 'Waris-Waris Simati'.obs;
  final kadPengenalanPemohon = TextEditingController().obs;
  final hubunganPemohon = 'Tiada Maklumat'.obs;
  final telefon = TextEditingController().obs;
  final alamat = TextEditingController().obs;
  final imagePemohon = TextEditingController().obs;
  final namaSimati = TextEditingController().obs;
  final kadPengenalanSimati = TextEditingController().obs;
  final tarikhKematian = TextEditingController().obs;

  ApplicationRepository applicationRepo = ApplicationRepository();

  getAllApplication() async {
    applicationList.clear();
    loading(true);
    String token = AppStorage.storage.read('access_token');
    List<Application> list = await applicationRepo.getAllApplication(token);
    if (list.isNotEmpty) {
      applicationList.assignAll(list);
    }
    loading(false);
  }

  getApplication() async {
    String token = AppStorage.storage.read('access_token');
    String applicationId = AppStorage.storage.read('application_id').toString();
    Application data = await applicationRepo.getApplication(
      {'application_id': applicationId},
      token,
    );
    if (data != null) {
      application(data);
    }
  }

  createApplication(file1, file2, file3) async {
    String token = AppStorage.storage.read('access_token');

    await applicationRepo.createApplication(
      token,
      file1,
      file2,
      file3,
      tarafKepentingan.value,
      namaPemohon.value.text,
      kadPengenalanPemohon.value.text,
      hubunganPemohon.value,
      alamat.value.text,
      telefon.value.text,
      namaSimati.value.text,
      kadPengenalanSimati.value.text,
      tarikhKematian.value.text,
    );

    namaPemohon.value.text = '';
    kadPengenalanPemohon.value.text = '';
    hubunganPemohon.value = 'Tiada Maklumat';
    telefon.value.text = '';
    alamat.value.text = '';
    namaSimati.value.text = '';
    kadPengenalanSimati.value.text = '';
    tarikhKematian.value.text = '';
  }

  deleteApplication(id) async {
    String token = AppStorage.storage.read('access_token');
    await applicationRepo.deleteApplication(
      {'application_id': id.toString()},
      token,
    );
    await getAllApplication();
  }

  updateApplication() async {
    String token = AppStorage.storage.read('access_token');
    String applicationId = AppStorage.storage.read('application_id').toString();
    await applicationRepo.updateApplication({'id': applicationId}, token);
    await getAllApplication();
  }
}
