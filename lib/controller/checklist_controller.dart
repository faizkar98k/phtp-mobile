import 'package:get/get.dart';
import 'package:phtp_mobile/data/model/checklist.dart';
import 'package:phtp_mobile/data/repository/checklist_repository.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';

class ChecklistController extends GetxController {
  final checkList = List<CheckList>().obs;

  ChecklistRepository checklistRepo = ChecklistRepository();

  getCheckList(applicationId) async {
    String token = AppStorage.storage.read('access_token');

    List<CheckList> list =
        await checklistRepo.getCheckList(token, applicationId.toString());

    if (list != null) {
      if (list.isNotEmpty) {
        checkList.assignAll(list);
      }
    }
  }

  updateCheckList(id, applicationId) async {
    String token = AppStorage.storage.read('access_token');

    await checklistRepo.updateCheckList(
      token,
      {'id': id.toString()},
    );

    await getCheckList(applicationId);
  }
}
