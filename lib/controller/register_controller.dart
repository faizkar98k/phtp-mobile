import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/data/model/user.dart';
import 'package:phtp_mobile/data/repository/account_repository.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/view/auth/login.dart';

class RegisterController extends GetxController {
  final nameController = TextEditingController().obs;
  final emailController = TextEditingController().obs;
  final phoneController = TextEditingController().obs;
  final addressController = TextEditingController().obs;
  final passwordController = TextEditingController().obs;

  AccountRepository accountRepository = AccountRepository();

  register(context) async {
    User user = await accountRepository.register({
      'name': nameController.value.text,
      'email': emailController.value.text,
      'phone_no': phoneController.value.text,
      'address': addressController.value.text,
      'password': passwordController.value.text,
    });

    if (user != null) {
      AppModal.showInformation(
        context,
        'Berjaya',
        'Pendaftaran anda berjaya log masuk sekarang.',
        onTap: () => AppRoute.push(context, Login()),
      );
    } else {
      AppModal.showInformation(
        context,
        'Tidak Berjaya',
        'Emel telah diambil oleh orang lain. Sila pilih emel yang berlainan',
      );
    }
  }
}
