import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/appointment.dart';
import 'package:phtp_mobile/data/repository/booking_repository.dart';

class BookingController extends GetxController {
  final name = TextEditingController().obs;
  final email = TextEditingController().obs;
  final people = TextEditingController().obs;
  final date = TextEditingController().obs;
  final purpose = 'Proses Penyaksian'.obs;
  final bookingList = List<Appointment>().obs;
  final loading = false.obs;

  BookingRepository bookingRepo = BookingRepository();

  createBooking() async {
    String token = AppStorage.storage.read('access_token');

    await bookingRepo.createBooking(
      {
        'name': name.value.text,
        'email': email.value.text,
        'appointment_purpose': purpose.value,
        'no_people': people.value.text,
        'appointment_date': date.value.text,
      },
      token,
    );

    name.value.clear();
    email.value.clear();
    people.value.clear();
    date.value.clear();
    purpose.value = 'Proses Penyaksian';
  }

  getBooking() async {
    bookingList.clear();
    loading(true);
    String token = AppStorage.storage.read('access_token');

    List<Appointment> list = await bookingRepo.getBooking(token);
    if (list.isNotEmpty) {
      bookingList.assignAll(list);
    }

    loading(false);
  }

  deleteBooking(id) async {
    String token = AppStorage.storage.read('access_token');
    await bookingRepo.deleteBooking({'id': id.toString()}, token);
  }
}
