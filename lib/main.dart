import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/view/auth/login.dart';
import 'package:phtp_mobile/view/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  await FlutterDownloader.initialize(debug: true);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          // textTheme: Theme.of(context).textTheme.apply(
          //       fontFamily: 'OpenSans',
          //     ),
          ),
      home: AppStorage.storage.read('access_token') == null ? Login() : Home(),
    );
  }
}
