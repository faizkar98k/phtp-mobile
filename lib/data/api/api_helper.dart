import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:phtp_mobile/utils/app_urls.dart';

class APIhelper {
  static const TIME_OUT = 15;

  setupHeader({String accessToken}) {
    if (accessToken != null) {
      return {
        'Accept': "application/json",
        'Authorization': "Bearer $accessToken",
      };
    } else {
      return {
        'Accept': "application/json",
      };
    }
  }

  Future<T> getRequest<T>({
    @required String path,
    Map<String, dynamic> body,
    String accessToken,
  }) async {
    try {
      final response = await http.get(
        apiEndpoint + path,
        headers: setupHeader(accessToken: accessToken),
      );

      return returnResponse<T>(response);
    } catch (e) {}
  }

  T returnResponse<T>(http.Response response) {
    if (response == null || response.statusCode == null) {}

    switch (response.statusCode) {
      case 200:
      case 201:
        return jsonDecode(response.body);
      default:
        break;
    }
  }
}
