import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:phtp_mobile/data/model/appointment.dart';
import 'package:phtp_mobile/utils/app_urls.dart';

class BookingRepository {
  Future createBooking(
    Map<String, dynamic> body,
    String token,
  ) async {
    await http.post(
      apiEndpoint + 'booking/booking',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      body: body,
    );
  }

  Future<List<Appointment>> getBooking(String token) async {
    final response = await http.get(
      apiEndpoint + 'booking/booking',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      List list = res["data"];

      return list.map((p) => Appointment.fromJson(p)).toList();
    } else {
      return null;
    }
  }

  Future deleteBooking(
    Map<String, dynamic> body,
    String token,
  ) async {
    final response = await http.post(
      apiEndpoint + 'booking/delete-booking',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );

    if (response.statusCode == 200) {}
  }
}
