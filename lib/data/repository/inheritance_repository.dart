import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:phtp_mobile/data/model/inheritance.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:path/path.dart' as pathy;
import 'package:async/async.dart';

class InheritanceRepository {
  Future createInheritance(
    token,
    file,
    noLot,
    noHakMilik,
    alamat,
    applicationId,
  ) async {
    final response = http.MultipartRequest(
      "POST",
      Uri.parse('${apiEndpoint}application/create-inheritance'),
    );
    response.headers['Authorization'] = "Bearer " + token;
    response.headers['Accept'] = 'application/json';
    response.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    // ignore: deprecated_member_use
    var stream = new http.ByteStream(DelegatingStream.typed(file.openRead()));

    var length = await file.length();

    var multipartFile = new http.MultipartFile(
      'attachment',
      stream,
      length,
      filename: pathy.basename(file.path),
    );

    response.fields['no_hak_milik'] = noHakMilik;
    response.fields['no_lot'] = noLot;
    response.fields['alamat'] = alamat;
    response.fields['application_id'] = applicationId;
    response.files.add(multipartFile);

    final res = await response.send();
    var value = await res.stream.single;

    if (res.statusCode == 200) {
      var res = json.decode(utf8.decode(value));
    }
  }

  Future<List<Inheritance>> getInheritance(
    Map<String, dynamic> body,
    String token,
  ) async {
    final response = await http.post(
      apiEndpoint + 'application/inheritance',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      List list = res["data"];
      return list.map((p) => Inheritance.fromJson(p)).toList();
    } else {
      return null;
    }
  }

  Future deleteInheritance(Map<String, dynamic> body, String token) async {
    await http.post(
      apiEndpoint + 'application/delete-inheritance',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );
  }

  Future updateInheritance(
    token,
    file,
    noLot,
    noHakMilik,
    alamat,
    id,
  ) async {
    final response = http.MultipartRequest(
      "POST",
      Uri.parse('${apiEndpoint}application/update-inheritance'),
    );
    response.headers['Authorization'] = "Bearer " + token;
    response.headers['Accept'] = 'application/json';
    response.headers['Content-Type'] = 'application/x-www-form-urlencoded';

    if (file != null) {
      // ignore: deprecated_member_use
      var stream = new http.ByteStream(DelegatingStream.typed(file.openRead()));
      var length = await file.length();
      var multipartFile = new http.MultipartFile(
        'attachment',
        stream,
        length,
        filename: pathy.basename(file.path),
      );
      response.files.add(multipartFile);
    }
    // ignore: deprecated_member_use

    response.fields['no_hak_milik'] = noHakMilik;
    response.fields['no_lot'] = noLot;
    response.fields['alamat'] = alamat;
    response.fields['id'] = id;

    final res = await response.send();
    var value = await res.stream.single;

    if (res.statusCode == 200) {
      var res = json.decode(utf8.decode(value));
    }
  }
}
