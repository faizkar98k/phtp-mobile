import 'dart:convert';

import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/user.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:http/http.dart' as http;

class AccountRepository {
  Future<User> login(Map<String, dynamic> body) async {
    final response = await http.post(
      apiEndpoint + 'login',
      headers: {
        'Accept': 'application/json',
      },
      body: body,
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);

      AppStorage.storage.write('access_token', res['data']['token']);

      return User.fromJson(res['data']['user']);
    } else {
      return null;
    }
  }

  Future<User> register(Map<String, dynamic> body) async {
    final response = await http.post(
      apiEndpoint + 'register',
      body: body,
      headers: {'Accept': 'application/json'},
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);

      return User.fromJson(res['data']);
    } else {
      return null;
    }
  }

  Future logout() async {
    await http.post(
      apiEndpoint + 'account/logout',
      headers: {'Accept': 'application/json'},
    );
  }

  Future<bool> changePassword(Map<String, dynamic> body, String token) async {
    final response = await http.post(
      apiEndpoint + 'account/change-password',
      body: body,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future updateProfile(Map<String, dynamic> body, String token) async {
    final response = await http.patch(
      apiEndpoint + 'account/update-profile',
      body: body,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return true;
    }
  }

  Future<User> getUserProfile(String token) async {
    final response = await http.get(
      apiEndpoint + 'account/profile',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);

      return User.fromJson(res['data']);
    } else {
      return null;
    }
  }
}
