import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/data/model/benificiary.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:path/path.dart' as pathy;
import 'package:async/async.dart';

class ApplicationRepository {
  Future getAllApplication(String accessToken) async {
    final response = await http.get(
      apiEndpoint + 'application/application-list',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + accessToken,
      },
    );
    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      List list = res["data"];
      return list.map((p) => Application.fromJson(p)).toList();
    }
  }

  Future createApplication(
    token,
    file1,
    file2,
    file3,
    tarafKepentingan,
    namaPemohon,
    kadPengenalanPemohon,
    hubunganPemohon,
    alamatPemohon,
    telefon,
    namaSimati,
    kadPengenalanSimati,
    tarikhKematian,
  ) async {
    final response = http.MultipartRequest(
      "POST",
      Uri.parse('${apiEndpoint}application/create-application'),
    );
    response.headers['Authorization'] = "Bearer " + token;
    response.headers['Accept'] = 'application/json';
    // response.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    // ignore: deprecated_member_use
    var stream = new http.ByteStream(DelegatingStream.typed(file1.openRead()));
    // ignore: deprecated_member_use
    var stream2 = new http.ByteStream(DelegatingStream.typed(file2.openRead()));
    // ignore: deprecated_member_use
    var stream3 = new http.ByteStream(DelegatingStream.typed(file3.openRead()));

    var length = await file1.length();
    var length2 = await file2.length();
    var length3 = await file3.length();

    var multipartFile = new http.MultipartFile(
      'attachment_pemohon',
      stream,
      length,
      filename: pathy.basename(file1.path),
    );
    var multipartFile2 = new http.MultipartFile(
      'attachment_simati',
      stream2,
      length2,
      filename: pathy.basename(file2.path),
    );

    var multipartFile3 = new http.MultipartFile(
      'attachment_sijil',
      stream3,
      length3,
      filename: pathy.basename(file3.path),
    );
    response.fields['taraf_kepentingan'] = tarafKepentingan;
    response.fields['nama_pemohon'] = namaPemohon;
    response.fields['kad_pengenalan_pemohon'] = kadPengenalanPemohon;
    response.fields['hubungan_pemohon'] = hubunganPemohon;
    response.fields['alamat_pemohon'] = alamatPemohon;
    response.fields['telefon'] = telefon;
    response.fields['nama_simati'] = namaSimati;
    response.fields['kad_pengenalan_simati'] = kadPengenalanSimati;
    response.fields['tarikh_kematian'] = tarikhKematian;
    response.files.add(multipartFile);
    response.files.add(multipartFile2);
    response.files.add(multipartFile3);

    final res = await response.send();
    var value = await res.stream.single;

    if (res.statusCode == 200) {
      var res = json.decode(utf8.decode(value));
      AppStorage.storage.write('application_id', res['data']['id']);
    }
  }

  Future<Application> getApplication(
    Map<String, dynamic> body,
    String token,
  ) async {
    final response = await http.post(
      apiEndpoint + 'application/application',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );
    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      return Application.fromJson(res['data']);
    } else {
      return null;
    }
  }

  Future deleteApplication(
    Map<String, dynamic> body,
    String token,
  ) async {
    await http.post(
      apiEndpoint + 'application/delete-application',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );
  }

  Future updateApplication(
    Map<String, dynamic> body,
    String token,
  ) async {
    await http.patch(
      apiEndpoint + 'application/update-application',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );
  }

  Future createBenificiary(
    token,
    file,
    name,
    hubungan,
    kadPengenalan,
    alamat,
    telefon,
    applicationId,
  ) async {
    final response = http.MultipartRequest(
      "POST",
      Uri.parse('${apiEndpoint}application/create-benificiary'),
    );
    response.headers['Authorization'] = "Bearer " + token;
    response.headers['Accept'] = 'application/json';
    response.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    // ignore: deprecated_member_use
    var stream = new http.ByteStream(DelegatingStream.typed(file.openRead()));

    var length = await file.length();

    var multipartFile = new http.MultipartFile(
      'attachment',
      stream,
      length,
      filename: pathy.basename(file.path),
    );

    response.fields['nama_waris'] = name;
    response.fields['hubungan_waris'] = hubungan;
    response.fields['kad_pengenalan_waris'] = kadPengenalan;
    response.fields['alamat_waris'] = alamat;
    response.fields['telefon'] = telefon;
    response.fields['application_id'] = applicationId;
    response.files.add(multipartFile);

    final res = await response.send();
    var value = await res.stream.single;

    if (res.statusCode == 200) {
      var res = json.decode(utf8.decode(value));
    }
  }

  Future<List<Benificiary>> getBenificiary(
    Map<String, dynamic> body,
    String accessToken,
  ) async {
    final response = await http.post(
      apiEndpoint + 'application/benificiary',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + accessToken,
      },
      body: body,
    );
    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      List list = res["data"];
      return list.map((p) => Benificiary.fromJson(p)).toList();
    } else {
      return null;
    }
  }

  Future deleteBenificiary(
    Map<String, dynamic> body,
    String accessToken,
  ) async {
    await http.post(
      apiEndpoint + 'application/delete-benificiary',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + accessToken,
      },
      body: body,
    );
  }

  Future updateBenificiary(
    token,
    file,
    name,
    hubungan,
    kadPengenalan,
    alamat,
    telefon,
    id,
  ) async {
    final response = http.MultipartRequest(
      "POST",
      Uri.parse('${apiEndpoint}application/update-benificiary'),
    );
    response.headers['Authorization'] = "Bearer " + token;
    response.headers['Accept'] = 'application/json';
    response.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    if (file != null) {
      // ignore: deprecated_member_use
      var stream = new http.ByteStream(DelegatingStream.typed(file.openRead()));

      var length = await file.length();

      var multipartFile = new http.MultipartFile(
        'attachment',
        stream,
        length,
        filename: pathy.basename(file.path),
      );
      response.files.add(multipartFile);
    }

    response.fields['nama_waris'] = name;
    response.fields['hubungan_waris'] = hubungan;
    response.fields['kad_pengenalan_waris'] = kadPengenalan;
    response.fields['alamat_waris'] = alamat;
    response.fields['telefon'] = telefon;
    response.fields['id'] = id;

    final res = await response.send();
    var value = await res.stream.single;

    if (res.statusCode == 200) {
      var res = json.decode(utf8.decode(value));
    }
  }
}
