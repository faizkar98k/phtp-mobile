import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:phtp_mobile/data/model/checklist.dart';
import 'package:phtp_mobile/utils/app_urls.dart';

class ChecklistRepository {
  Future getCheckList(String token, String applicationId) async {
    final response = await http.post(
      apiEndpoint + 'checklist/getChecklist',
      headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json',
      },
      body: {
        "application_id": applicationId,
      },
    );

    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      List list = res["data"];

      return list.map((p) => CheckList.fromJson(p)).toList();
    }
  }

  Future updateCheckList(
    String token,
    Map<String, dynamic> body,
  ) async {
    await http.post(
      apiEndpoint + 'checklist/checklist',
      headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json',
      },
      body: body,
    );
  }
}
