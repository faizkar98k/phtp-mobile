// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'benificiary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Benificiary _$BenificiaryFromJson(Map<String, dynamic> json) {
  return Benificiary(
    id: json['id'] as int,
    applicationId: json['application_id'] as String,
    namaWaris: json['nama_waris'] as String,
    hubunganWaris: json['hubungan_waris'] as String,
    kadPengenalanWaris: json['kad_pengenalan_waris'] as String,
    alamatWaris: json['alamat_waris'] as String,
    telefon: json['telefon'] as String,
    attachment: json['attachment'] as String,
    attachmentImgStoredPath: json['attachment_img_stored_path'] as String,
    attachmentImgMimeType: json['attachment_img_mime_type'] as String,
  );
}

Map<String, dynamic> _$BenificiaryToJson(Benificiary instance) =>
    <String, dynamic>{
      'id': instance.id,
      'application_id': instance.applicationId,
      'nama_waris': instance.namaWaris,
      'hubungan_waris': instance.hubunganWaris,
      'kad_pengenalan_waris': instance.kadPengenalanWaris,
      'alamat_waris': instance.alamatWaris,
      'telefon': instance.telefon,
      'attachment': instance.attachment,
      'attachment_img_stored_path': instance.attachmentImgStoredPath,
      'attachment_img_mime_type': instance.attachmentImgMimeType,
    };
