import 'package:json_annotation/json_annotation.dart';
import 'package:phtp_mobile/data/model/benificiary.dart';
import 'package:phtp_mobile/data/model/inheritance.dart';

part 'application.g.dart';

@JsonSerializable()
class Application {
  int id;
  @JsonKey(name: 'reference_id')
  String referenceId;
  String status;
  @JsonKey(name: 'nama_pemohon')
  String namaPemohon;
  @JsonKey(name: 'kad_pengenalan_pemohon')
  String kadPengenalanPemohon;
  @JsonKey(name: 'hubungan_pemohon')
  String hubunganPemohon;
  @JsonKey(name: 'alamat_pemohon')
  String alamatPemohon;
  String telefon;
  @JsonKey(name: 'nama_simati')
  String namaSimati;
  @JsonKey(name: 'kad_pengenalan_simati')
  String kadPengenalanSimati;
  @JsonKey(name: 'attachment_pemohon')
  String attachmentPemohon;
  @JsonKey(name: 'attachment_simati')
  String attachmentSimati;
  @JsonKey(name: 'attachment_sijil')
  String attachmentSijil;
  @JsonKey(name: 'tarikh_kematian')
  String tarikhKematian;
  @JsonKey(name: 'created_at')
  String createdAt;
  @JsonKey(name: 'attachment_img_stored_path_pemohon')
  String attachmentImgStoredPathPemohon;
  @JsonKey(name: "attachment_img_stored_path_simati")
  String attachmentImgStoredPathSimati;
  @JsonKey(name: "attachment_img_stored_path_sijil")
  String attachmentImgStoredPathSijil;
  @JsonKey(name: "attachment_img_mime_type_pemohon")
  String attachmentImgMimeTypePemohon;
  @JsonKey(name: "attachment_img_mime_type_simati")
  String attachmentImgMimeTypeSimati;
  @JsonKey(name: "attachment_img_mime_type_sijil")
  String attachmentImgMimeTypeSijil;
  @JsonKey(name: "tempat_pembicaraan")
  String tempatPembicaraan;
  @JsonKey(name: "pembicaraan_date")
  String pembicaraanDate;
  @JsonKey(name: "pembicaraan_time")
  String pembicaraanTime;

  List<Benificiary> benificiary;
  List<Inheritance> inheritance;

  Application({
    this.id,
    this.status,
    this.namaPemohon,
    this.kadPengenalanPemohon,
    this.hubunganPemohon,
    this.alamatPemohon,
    this.telefon,
    this.namaSimati,
    this.kadPengenalanSimati,
    this.tarikhKematian,
    this.benificiary,
    this.inheritance,
    this.attachmentPemohon,
    this.attachmentSimati,
    this.attachmentSijil,
    this.referenceId,
    this.createdAt,
    this.attachmentImgStoredPathPemohon,
    this.attachmentImgStoredPathSimati,
    this.attachmentImgMimeTypePemohon,
    this.attachmentImgStoredPathSijil,
    this.attachmentImgMimeTypeSijil,
    this.attachmentImgMimeTypeSimati,
    this.tempatPembicaraan,
    this.pembicaraanDate,
    this.pembicaraanTime,
  });

  factory Application.fromJson(Map<String, dynamic> json) =>
      _$ApplicationFromJson(json);

  Map<String, dynamic> toJson() => _$ApplicationToJson(this);
}
