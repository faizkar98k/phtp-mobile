import 'package:json_annotation/json_annotation.dart';
import 'package:phtp_mobile/data/model/user_profile.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  int id;
  String name;
  String email;
  String status;
  UserProfile profile;

  User({
    this.id,
    this.name,
    this.email,
    this.status,
    this.profile,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
