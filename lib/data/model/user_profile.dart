import 'package:json_annotation/json_annotation.dart';
import 'package:phtp_mobile/utils/app_urls.dart';

part 'user_profile.g.dart';

@JsonSerializable()
class UserProfile {
  @JsonKey(name: 'first_name')
  String firstName;
  @JsonKey(name: 'last_name')
  String lastName;
  @JsonKey(name: 'phone_no')
  String phoneNo;
  String address;

  UserProfile({
    this.firstName,
    this.lastName,
    this.phoneNo,
    this.address,
  });

  getProfileImageUrl() {
    String imageURL;
    if (imageURL != null) {
      return endpoint + imageURL;
    } else {
      return null;
    }
  }

  factory UserProfile.fromJson(Map<String, dynamic> json) =>
      _$UserProfileFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileToJson(this);
}
