// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appointment _$AppointmentFromJson(Map<String, dynamic> json) {
  return Appointment(
    id: json['id'] as int,
    status: json['status'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    purpose: json['appointment_purpose'] as String,
    people: json['no_people'] as String,
    date: json['appointment_date'] as String,
  );
}

Map<String, dynamic> _$AppointmentToJson(Appointment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'name': instance.name,
      'email': instance.email,
      'appointment_purpose': instance.purpose,
      'no_people': instance.people,
      'appointment_date': instance.date,
    };
