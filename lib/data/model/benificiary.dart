import 'package:json_annotation/json_annotation.dart';

part 'benificiary.g.dart';

@JsonSerializable()
class Benificiary {
  int id;
  @JsonKey(name: 'application_id')
  String applicationId;
  @JsonKey(name: 'nama_waris')
  String namaWaris;
  @JsonKey(name: 'hubungan_waris')
  String hubunganWaris;
  @JsonKey(name: 'kad_pengenalan_waris')
  String kadPengenalanWaris;
  @JsonKey(name: 'alamat_waris')
  String alamatWaris;
  String telefon;
  String attachment;
  @JsonKey(name: 'attachment_img_stored_path')
  String attachmentImgStoredPath;
  @JsonKey(name: 'attachment_img_mime_type')
  String attachmentImgMimeType;

  Benificiary({
    this.id,
    this.applicationId,
    this.namaWaris,
    this.hubunganWaris,
    this.kadPengenalanWaris,
    this.alamatWaris,
    this.telefon,
    this.attachment,
    this.attachmentImgStoredPath,
    this.attachmentImgMimeType,
  });

  factory Benificiary.fromJson(Map<String, dynamic> json) =>
      _$BenificiaryFromJson(json);

  Map<String, dynamic> toJson() => _$BenificiaryToJson(this);
}
