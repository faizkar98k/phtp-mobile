import 'package:json_annotation/json_annotation.dart';

part 'appointment.g.dart';

@JsonSerializable()
class Appointment {
  int id;
  String status;
  String name;
  String email;
  @JsonKey(name: 'appointment_purpose')
  String purpose;
  @JsonKey(name: 'no_people')
  String people;
  @JsonKey(name: 'appointment_date')
  String date;

  Appointment({
    this.id,
    this.status,
    this.name,
    this.email,
    this.purpose,
    this.people,
    this.date,
  });

  factory Appointment.fromJson(Map<String, dynamic> json) =>
      _$AppointmentFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentToJson(this);
}
