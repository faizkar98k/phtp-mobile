// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inheritance.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Inheritance _$InheritanceFromJson(Map<String, dynamic> json) {
  return Inheritance(
    id: json['id'] as int,
    applicationId: json['application_id'] as String,
    noHakMilik: json['no_hak_milik'] as String,
    noLot: json['no_lot'] as String,
    alamat: json['alamat'] as String,
    attachment: json['attachment'] as String,
    attachmentImgMimeType: json['attachment_img_mime_type'] as String,
    attachmentImgStoredPath: json['attachment_img_stored_path'] as String,
  );
}

Map<String, dynamic> _$InheritanceToJson(Inheritance instance) =>
    <String, dynamic>{
      'id': instance.id,
      'application_id': instance.applicationId,
      'no_hak_milik': instance.noHakMilik,
      'no_lot': instance.noLot,
      'alamat': instance.alamat,
      'attachment': instance.attachment,
      'attachment_img_stored_path': instance.attachmentImgStoredPath,
      'attachment_img_mime_type': instance.attachmentImgMimeType,
    };
