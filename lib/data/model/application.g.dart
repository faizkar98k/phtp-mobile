// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'application.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Application _$ApplicationFromJson(Map<String, dynamic> json) {
  return Application(
    id: json['id'] as int,
    status: json['status'] as String,
    namaPemohon: json['nama_pemohon'] as String,
    kadPengenalanPemohon: json['kad_pengenalan_pemohon'] as String,
    hubunganPemohon: json['hubungan_pemohon'] as String,
    alamatPemohon: json['alamat_pemohon'] as String,
    telefon: json['telefon'] as String,
    namaSimati: json['nama_simati'] as String,
    kadPengenalanSimati: json['kad_pengenalan_simati'] as String,
    tarikhKematian: json['tarikh_kematian'] as String,
    benificiary: (json['benificiary'] as List)
        ?.map((e) =>
            e == null ? null : Benificiary.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    inheritance: (json['inheritance'] as List)
        ?.map((e) =>
            e == null ? null : Inheritance.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    attachmentPemohon: json['attachment_pemohon'] as String,
    attachmentSimati: json['attachment_simati'] as String,
    attachmentSijil: json['attachment_sijil'] as String,
    referenceId: json['reference_id'] as String,
    createdAt: json['created_at'] as String,
    attachmentImgStoredPathPemohon:
        json['attachment_img_stored_path_pemohon'] as String,
    attachmentImgStoredPathSimati:
        json['attachment_img_stored_path_simati'] as String,
    attachmentImgMimeTypePemohon:
        json['attachment_img_mime_type_pemohon'] as String,
    attachmentImgStoredPathSijil:
        json['attachment_img_stored_path_sijil'] as String,
    attachmentImgMimeTypeSijil:
        json['attachment_img_mime_type_sijil'] as String,
    attachmentImgMimeTypeSimati:
        json['attachment_img_mime_type_simati'] as String,
    tempatPembicaraan: json['tempat_pembicaraan'] as String,
    pembicaraanDate: json['pembicaraan_date'] as String,
    pembicaraanTime: json['pembicaraan_time'] as String,
  );
}

Map<String, dynamic> _$ApplicationToJson(Application instance) =>
    <String, dynamic>{
      'id': instance.id,
      'reference_id': instance.referenceId,
      'status': instance.status,
      'nama_pemohon': instance.namaPemohon,
      'kad_pengenalan_pemohon': instance.kadPengenalanPemohon,
      'hubungan_pemohon': instance.hubunganPemohon,
      'alamat_pemohon': instance.alamatPemohon,
      'telefon': instance.telefon,
      'nama_simati': instance.namaSimati,
      'kad_pengenalan_simati': instance.kadPengenalanSimati,
      'attachment_pemohon': instance.attachmentPemohon,
      'attachment_simati': instance.attachmentSimati,
      'attachment_sijil': instance.attachmentSijil,
      'tarikh_kematian': instance.tarikhKematian,
      'created_at': instance.createdAt,
      'attachment_img_stored_path_pemohon':
          instance.attachmentImgStoredPathPemohon,
      'attachment_img_stored_path_simati':
          instance.attachmentImgStoredPathSimati,
      'attachment_img_stored_path_sijil': instance.attachmentImgStoredPathSijil,
      'attachment_img_mime_type_pemohon': instance.attachmentImgMimeTypePemohon,
      'attachment_img_mime_type_simati': instance.attachmentImgMimeTypeSimati,
      'attachment_img_mime_type_sijil': instance.attachmentImgMimeTypeSijil,
      'tempat_pembicaraan': instance.tempatPembicaraan,
      'pembicaraan_date': instance.pembicaraanDate,
      'pembicaraan_time': instance.pembicaraanTime,
      'benificiary': instance.benificiary,
      'inheritance': instance.inheritance,
    };
