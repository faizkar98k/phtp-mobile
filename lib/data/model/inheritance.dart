import 'package:json_annotation/json_annotation.dart';

part 'inheritance.g.dart';

@JsonSerializable()
class Inheritance {
  int id;
  @JsonKey(name: 'application_id')
  String applicationId;
  @JsonKey(name: 'no_hak_milik')
  String noHakMilik;
  @JsonKey(name: 'no_lot')
  String noLot;
  @JsonKey(name: 'alamat')
  String alamat;
  String attachment;
  @JsonKey(name: 'attachment_img_stored_path')
  String attachmentImgStoredPath;
  @JsonKey(name: 'attachment_img_mime_type')
  String attachmentImgMimeType;

  Inheritance({
    this.id,
    this.applicationId,
    this.noHakMilik,
    this.noLot,
    this.alamat,
    this.attachment,
    this.attachmentImgMimeType,
    this.attachmentImgStoredPath,
  });

  factory Inheritance.fromJson(Map<String, dynamic> json) =>
      _$InheritanceFromJson(json);

  Map<String, dynamic> toJson() => _$InheritanceToJson(this);
}
