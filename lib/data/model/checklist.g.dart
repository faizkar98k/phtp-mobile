// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checklist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckList _$CheckListFromJson(Map<String, dynamic> json) {
  return CheckList(
    id: json['id'] as int,
    title: json['title'] as String,
    status: json['status'] as int,
  );
}

Map<String, dynamic> _$CheckListToJson(CheckList instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'status': instance.status,
    };
