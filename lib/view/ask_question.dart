import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class AskQuestion extends StatelessWidget {
  const AskQuestion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Tanya Soalan',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              AppTextFieldWithLabel(
                label: 'Nama Penuh',
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppTextFieldWithLabel(
                label: 'Emel',
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppTextFieldWithLabel(
                label: 'Soalan',
                keyboardType: TextInputType.multiline,
              ),
              SizedBox(
                height: AppSize.spaceX24,
              ),
              AppButton(
                btnTitle: 'Hantar',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
