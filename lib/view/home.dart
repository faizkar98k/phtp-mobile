import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
      
  int selectedIndex;

  Home({this.selectedIndex = 0});
}

class _HomeState extends State<Home> {
  AccountController accountCont = Get.put(AccountController());
  void onItemTapped(int index) {
    setState(() {
      widget.selectedIndex = index;
    });
  }

  @override
  void initState() {
    accountCont.getUserProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      hasDrawer: true,
      title: pageTitle[widget.selectedIndex],
      hasBottomNavigation: true,
      bottomNavigationBar: BottomNavigationBar(
        iconSize: AppSize.spaceX24,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_filled,
            ),
            label: 'MyPermohonan',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              EvaIcons.video,
            ),
            label: 'Meeting',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Profil',
          ),
        ],
        currentIndex: widget.selectedIndex,
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.grayColor,
        selectedFontSize: 12,
        selectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w500,
        ),
        onTap: onItemTapped,
      ),
      child: page[widget.selectedIndex],
    );
  }
}
