import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/controller/booking_controller.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/app_validation.dart';
import 'package:phtp_mobile/view/booking/bookings.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/input/app_date_time_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_select_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class CreateBooking extends StatefulWidget {
  @override
  _CreateBookingState createState() => _CreateBookingState();
}

class _CreateBookingState extends State<CreateBooking> {
  GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  BookingController bookingCont = Get.put(BookingController());
  AccountController accountCont = Get.put(AccountController());

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Temu Janji Baru',
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(
            left: AppSize.spaceX16,
            right: AppSize.spaceX16,
            top: AppSize.spaceX24,
          ),
          child: Obx(
            () => Form(
              key: formKey5,
              child: Column(
                children: [
                  AppTextFieldWithLabel(
                    controller: bookingCont.name.value
                      ..text = accountCont.user.value.name,
                    label: 'Nama Penuh',
                    validatorFunction: (val) =>
                        val.isEmpty ? "Nama penuh tidak boleh kosong" : null,
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: bookingCont.email.value
                      ..text = accountCont.user.value.email,
                    label: 'Emel',
                    validatorFunction: (val) {
                      if (val.isEmpty) {
                        return 'Emel tidak boleh kosong';
                      } else if (!val.isEmail) {
                        return 'Emel dalam format yang salah';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppSelectField(
                    value: bookingCont.purpose.value,
                    items: ['Proses Penyaksian', 'Proses Cop'],
                    label: 'Tujuan Temu Janji',
                    onChange: (value) {
                      bookingCont.purpose(value);
                    },
                    hint: 'Pilih tujuan temu janji',
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: bookingCont.people.value,
                    label: 'Bilangan Orang',
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                    ],
                    keyboardType: TextInputType.number,
                    validatorFunction: (val) => val.isEmpty
                        ? "Bilangan orang tidak boleh kosong"
                        : null,
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppDateTimeField(
                    controller: bookingCont.date.value,
                    label: 'Tarikh',
                    validatorFunction: (val) =>
                        val == null ? "Tarikh tidak boleh kosong" : null,
                  ),
                  SizedBox(
                    height: AppSize.spaceX24,
                  ),
                  AppButton(
                    onTap: () {
                      if (formKey5.currentState.validate()) {
                        AppModal.showConfirmation(
                          context,
                          'Temu Janji',
                          'Adakah anda ingin membuat temu janji ini ?',
                          () async {
                            await bookingCont.createBooking();
                            AppRoute.pushReplacement(context, Bookings());
                          },
                        );
                      }
                    },
                    btnTitle: 'Hantar',
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
