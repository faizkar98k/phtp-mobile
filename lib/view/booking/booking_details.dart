import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/booking_controller.dart';
import 'package:phtp_mobile/data/model/appointment.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_badge.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';

class BookingDetails extends StatefulWidget {
  @override
  _BookingDetailsState createState() => _BookingDetailsState();

  final Appointment appointment;
  BookingDetails({this.appointment});
}

class _BookingDetailsState extends State<BookingDetails> {
  BookingController bookingCont = Get.put(BookingController());

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Butiran Temu Janji',
      action: InkWell(
        onTap: () {
          AppModal.showConfirmation(
            context,
            'Padam Temu Janji',
            'Adakah anda pasti ingin memadam temu janji ini ?',
            () async {
              await bookingCont.deleteBooking(widget.appointment.id);
              await bookingCont.getBooking();
              AppRoute.pop(context);
            },
          );
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Icon(
            EvaIcons.trash2Outline,
            color: AppColors.whiteColor,
          ),
        ),
      ),
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(
            top: 24,
            left: 16,
            right: 16,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText.appTitleText('nama'),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(widget.appointment.name ?? ''),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appTitleText('emel'),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(widget.appointment.email ?? ''),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appTitleText('tujuan temu janji'),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(widget.appointment.purpose ?? ''),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appTitleText('bilangan orang'),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(widget.appointment.people ?? ''),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appTitleText('tarikh'),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(widget.appointment.date ?? ''),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appTitleText('status'),
              SizedBox(
                height: 4,
              ),
              AppBadge(
                isSuccess: widget.appointment.status == 'approved' ?? false,
                title: widget.appointment.status ?? '',
              )
            ],
          ),
        ),
      ),
    );
  }
}
