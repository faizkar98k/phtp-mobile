import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/booking_controller.dart';
import 'package:phtp_mobile/utils/app_refresher.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/view/booking/booking_details.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_empty_data.dart';
import 'package:phtp_mobile/view/share_widget/app_loading.dart';
import 'package:phtp_mobile/view/share_widget/card/app_booking_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Bookings extends StatefulWidget {
  @override
  _BookingsState createState() => _BookingsState();
}

class _BookingsState extends State<Bookings> {
  RefreshController refreshController = RefreshController();
  BookingController bookingCont = Get.put(BookingController());

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await bookingCont.getBooking();
    });
    super.initState();
  }

  onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    await bookingCont.getBooking();
    refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Senarai Temu Janji',
      child: AppRefresher(
        controller: refreshController,
        onRefresh: onRefresh,
        enablePullDown: true,
        enablePullUp: false,
        child: Obx(
          () => bookingCont.loading.value
              ? AppLoading()
              : bookingCont.bookingList.isEmpty
                  ? AppEmptyData(
                      title: 'Tiada Temu Janji',
                      subttile: 'Anda tidak pernah buat temu janji.',
                    )
                  : ListView.builder(
                      itemCount: bookingCont.bookingList.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () => AppRoute.push(
                            context,
                            BookingDetails(
                              appointment: bookingCont.bookingList[index],
                            ),
                          ),
                          child: AppBookingCard(
                            title: bookingCont.bookingList[index].name,
                            subtitle1: bookingCont.bookingList[index].email,
                            subtitle2: bookingCont.bookingList[index].date,
                            status: bookingCont.bookingList[index].status,
                          ),
                        );
                      },
                    ),
        ),
      ),
    );
  }
}
