import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/booking_controller.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/card/app_card.dart';

class Booking extends StatefulWidget {
  @override
  _BookingState createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  BookingController bookingCont = Get.put(BookingController());

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await bookingCont.getBooking();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: AppSize.spaceX16,
        right: AppSize.spaceX16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: AppSize.spaceX24,
          ),
          Text(
            'Tempah Temu Janji',
            style: TextStyle(
              color: AppColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: AppSize.fontLargeX2,
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            'Lihat keputusan temu janji anda bersama pegawai tadbir.',
            style: TextStyle(
              color: AppColors.labelTextColor,
              fontSize: AppSize.fontMedium,
            ),
          ),
          SizedBox(
            height: AppSize.spaceX48,
          ),
          Expanded(
            child: StaggeredGridView.countBuilder(
              shrinkWrap: true,
              crossAxisCount: 4,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) {
                return Obx(
                  () => AppCard(
                    title: bookingCard[index]['title'],
                    subtitle: bookingCard[index]['subtitle'],
                    total: bookingCont.bookingList.length.toString(),
                    hasTotal: index == 1 ?? false,
                    onTap: () => AppRoute.push(
                      context,
                      bookingCard[index]['page'],
                    ),
                  ),
                );
              },
              staggeredTileBuilder: (int index) =>
                  StaggeredTile.count(2, index.isEven ? 2 : 2.5),
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
            ),
          )
        ],
      ),
    );
  }
}
