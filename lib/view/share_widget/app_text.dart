import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppText {
  static Widget appHeroText(String label) {
    return Text(
      label ?? '',
      style: TextStyle(
        color: AppColors.darkTextColor,
        fontSize: 18,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static Widget appTitleText(String label) {
    return Text(
      label.toUpperCase() ?? '',
      style: TextStyle(
        color: AppColors.grayColor,
        fontWeight: FontWeight.w800,
        fontSize: 11,
      ),
    );
  }

  static Widget appSubtitleText(String label) {
    return Text(
      label ?? '',
      style: TextStyle(
        color: AppColors.labelTextColor,
        fontSize: 16,
        fontWeight: FontWeight.w600,
      ),
    );
  }
}
