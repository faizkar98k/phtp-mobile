import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppBadge extends StatelessWidget {
  final String title;
  final bool isSuccess;

  AppBadge({
    this.title,
    this.isSuccess = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: AppSize.space,
        vertical: AppSize.space,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: isSuccess ? AppColors.successColor : AppColors.warningColor,
      ),
      child: Text(
        title.toUpperCase() ?? '',
        style: TextStyle(
          color: AppColors.whiteColor,
          fontWeight: FontWeight.bold,
          fontSize: 11.0,
        ),
      ),
    );
  }
}
