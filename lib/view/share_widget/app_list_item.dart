import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppListItem extends StatelessWidget {
  final Widget icon;
  final String title;
  final Color color;
  final Function onTap;
  final bool topDivider;
  final bool bottomDivider;
  final Widget trailing;
  final String subTitle;
  final double fontTitle;
  final double paddingHorizontal;

  const AppListItem({
    Key key,
    this.icon,
    this.title = '',
    @required this.onTap,
    this.trailing,
    this.subTitle,
    this.color = AppColors.labelTextColor,
    this.topDivider = false,
    this.bottomDivider = true,
    this.fontTitle = 14,
    this.paddingHorizontal = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double maxWidth = AppSize.maxWidthScreen(context);

    return Container(
      child: Column(
        children: [
          Visibility(
            visible: this.topDivider,
            child: Container(
              width: maxWidth,
              child: Divider(
                height: 0,
              ),
            ),
          ),
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: this.paddingHorizontal,
                vertical: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  this.icon ?? Container(),
                  SizedBox(
                    width: this.icon != null ? 20 : 0,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          this.title,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: this.color,
                            fontSize: this.fontTitle,
                            fontWeight: FontWeight.w600,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: AppSize.space,
                        ),
                        this.subTitle != null
                            ? Text(
                                this.subTitle,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: AppSize.fontMedium,
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: AppSize.spaceX1,
                  ),
                  this.trailing ??
                      Icon(
                        Icons.arrow_forward_ios,
                        color: this.color,
                        size: AppSize.fontLargeX2,
                      ),
                ],
              ),
            ),
            onTap: this.onTap,
          ),
          Visibility(
            visible: this.bottomDivider,
            child: Container(
              width: maxWidth,
              child: Divider(
                height: 0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
