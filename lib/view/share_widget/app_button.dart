import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppButton extends StatelessWidget {
  final String btnTitle;
  final Function onTap;

  AppButton({
    this.btnTitle,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap ?? null,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 54,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: AppColors.primaryColor,
        ),
        child: Center(
          child: Text(
            this.btnTitle ?? '',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: AppColors.whiteColor,
              fontSize: AppSize.fontLargeX2,
            ),
          ),
        ),
      ),
    );
  }
}
