import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(AppColors.primaryColor),
      ),
    );
  }
}
