import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: AppColors.grayColor,
    );
  }
}
