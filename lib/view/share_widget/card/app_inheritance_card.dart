import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_text_with_icon.dart';

class AppInheritanceCard extends StatelessWidget {
  final String title;
  final String subtitle1;
  final String subtitle2;
  final Function onDelete;

  AppInheritanceCard({
    this.title,
    this.subtitle1,
    this.subtitle2,
    this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSize.widthScreen(context),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: AppColors.lightColor,
          ),
        ),
      ),
      padding: EdgeInsets.all(
        AppSize.spaceX16,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                this.title ?? '',
                style: TextStyle(
                  color: AppColors.darkTextColor,
                  fontWeight: FontWeight.w500,
                  fontSize: AppSize.fontLarge,
                ),
              ),
              InkWell(
                onTap: () => AppModal.showConfirmation(
                  context,
                  'Padam Harta Tak Alih',
                  'Adakah anda pasti ingin membuang harta tak alih ini daripada senarai ?',
                  this.onDelete,
                ),
                child: Icon(
                  EvaIcons.trash2,
                  size: 18,
                  color: AppColors.dangerColor,
                ),
              )
            ],
          ),
          SizedBox(
            height: 8,
          ),
          AppTextWithIcon(
            title: this.subtitle1 ?? '',
            icon: EvaIcons.map,
          ),
          SizedBox(
            height: 8,
          ),
          AppTextWithIcon(
            title: this.subtitle2 ?? '',
            icon: EvaIcons.pin,
          ),
        ],
      ),
    );
  }
}
