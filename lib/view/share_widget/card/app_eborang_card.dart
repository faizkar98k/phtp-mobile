import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppEBorangCard extends StatelessWidget {
  final String title;
  final String subtitle;

  AppEBorangCard({this.title, this.subtitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
          color: AppColors.lightColor,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title ?? '',
                style: TextStyle(
                  color: AppColors.darkTextColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                subtitle ?? '',
                style: TextStyle(
                  color: AppColors.grayColor,
                  fontSize: 14,
                ),
              ),
            ],
          ),
          Icon(
            EvaIcons.downloadOutline,
            color: AppColors.darkBlueColor,
          ),
        ],
      ),
    );
  }
}
