import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppCard extends StatelessWidget {
  final String title;
  final String subtitle;
  final bool hasTotal;
  final String total;
  final Function onTap;

  AppCard({
    this.title,
    this.subtitle,
    this.hasTotal = false,
    this.total,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(AppSize.spaceX8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0),
          border: Border.all(
            width: 0.5,
            color: AppColors.darkBlueColor,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.title ?? '',
                  style: TextStyle(
                    color: AppColors.labelTextColor,
                    fontSize: AppSize.fontMedium,
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(
                  this.subtitle ?? '',
                  style: TextStyle(
                    color: AppColors.grayColor,
                    fontSize: AppSize.fontMedium,
                  ),
                ),
              ],
            ),
            this.hasTotal
                ? Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        color: AppColors.lightColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Text(
                          this.total ?? '',
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: AppSize.fontMedium,
                          ),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
