import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_badge.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';
import 'package:phtp_mobile/view/share_widget/app_text_with_icon.dart';

class AppApplicationCard extends StatelessWidget {
  final String title;
  final String subtitle1;
  final String subtitle2;
  final String status;
  final Function onTap;
  final bool isSuccess;
  final Function onDelete;
  final String tempatBicara;
  final String dateBicara;
  final String timeBicara;

  AppApplicationCard({
    this.title,
    this.subtitle1,
    this.subtitle2,
    this.status,
    this.onTap,
    this.isSuccess = true,
    this.onDelete,
    this.tempatBicara,
    this.dateBicara,
    this.timeBicara,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: AppSize.widthScreen(context),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: AppColors.lightColor,
            ),
          ),
        ),
        padding: EdgeInsets.all(
          AppSize.spaceX16,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  this.title ?? '',
                  style: TextStyle(
                    color: AppColors.darkTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: AppSize.fontLarge,
                  ),
                ),
                InkWell(
                  onTap: () {
                    AppModal.showConfirmation(
                      context,
                      'Padam Permohonan',
                      'Adakah anda pasti ingin padam permohonan ini daripada senarai ?',
                      this.onDelete,
                    );
                  },
                  child: Icon(
                    EvaIcons.trash2,
                    size: 18,
                    color: AppColors.dangerColor,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 8,
            ),
            AppTextWithIcon(
              title: this.subtitle1 ?? '',
              icon: EvaIcons.person,
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AppTextWithIcon(
                  title: this.subtitle2 ?? '',
                  icon: EvaIcons.calendar,
                ),
                AppBadge(
                  title: this.status.toUpperCase() ?? '',
                  isSuccess: this.isSuccess,
                )
              ],
            ),
            SizedBox(
              height: this.status == "pembicaraan" ? 16 : 8,
            ),
            this.status == "pembicaraan"
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '** PENTING **',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: AppColors.darkTextColor),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      AppText.appTitleText('Tempat Pembicaraan'),
                      SizedBox(
                        height: 4,
                      ),
                      AppText.appSubtitleText(this.tempatBicara ?? ''),
                      SizedBox(
                        height: AppSize.spaceX16,
                      ),
                      AppText.appTitleText('tarikh pembicaraan'),
                      AppText.appSubtitleText(
                        DateFormat("dd-MM-yyyy").format(
                          DateTime.parse(this.dateBicara ?? ''),
                        ),
                      ),
                      SizedBox(
                        height: AppSize.spaceX16,
                      ),
                      AppText.appTitleText('Masa pembicaraan'),
                      AppText.appSubtitleText(
                        DateFormat.jm().format(
                          DateTime.parse(
                            "2021-05-19 " + this.timeBicara + ":00.9123",
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 6.0,
                      ),
                      Text(
                        '* Pastikan semua ahli yang terlibat dapat hadir semasa proses pembicaraan.',
                        style: TextStyle(
                          color: AppColors.darkTextColor,
                        ),
                      ),
                    ],
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
