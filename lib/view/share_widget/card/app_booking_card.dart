import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_badge.dart';
import 'package:phtp_mobile/view/share_widget/app_text_with_icon.dart';

class AppBookingCard extends StatelessWidget {
  final String title;
  final String subtitle1;
  final String subtitle2;
  final String status;
  final IconData icon;

  AppBookingCard({
    this.title,
    this.subtitle1,
    this.subtitle2,
    this.status,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSize.widthScreen(context),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: AppColors.lightColor,
          ),
        ),
      ),
      padding: EdgeInsets.all(
        AppSize.spaceX16,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            this.title ?? '',
            style: TextStyle(
              color: AppColors.darkTextColor,
              fontWeight: FontWeight.w500,
              fontSize: AppSize.fontLarge,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          AppTextWithIcon(
            title: this.subtitle1 ?? '',
            icon: EvaIcons.email,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppTextWithIcon(
                title: this.subtitle2 ?? '',
                icon: EvaIcons.calendar,
              ),
              AppBadge(
                isSuccess: this.status == 'approved' ?? false,
                title: this.status.toUpperCase() ?? '',
              )
            ],
          ),
        ],
      ),
    );
  }
}
