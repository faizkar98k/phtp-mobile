import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppTextWithIcon extends StatelessWidget {
  final String title;
  final IconData icon;
  AppTextWithIcon({
    this.title,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          this.icon,
          color: AppColors.labelTextColor.withOpacity(0.6),
          size: 18,
        ),
        SizedBox(
          width: AppSize.spaceX8,
        ),
        Container(
          width: AppSize.widthScreen(context) * 0.55,
          child: Text(
            this.title ?? '',
            style: TextStyle(
              color: AppColors.labelTextColor.withOpacity(0.6),
              fontSize: AppSize.fontSmall,
            ),
          ),
        ),
      ],
    );
  }
}
