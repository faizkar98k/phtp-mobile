import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';

class AppApplicationReview extends StatelessWidget {
  final ApplicationController applicationCont =
      Get.put(ApplicationController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText.appHeroText('Maklumat Pemohon'),
          SizedBox(
            height: AppSize.spaceX24,
          ),
          AppText.appTitleText('nama pemohon'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(
            applicationCont.namaPemohon.value.text,
          ),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('no kad pengenalan'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(
            applicationCont.kadPengenalanPemohon.value.text,
          ),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('hubungan dengan simati'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(applicationCont.hubunganPemohon.value),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('No Telefon'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(applicationCont.telefon.value.text),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('alamat'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(applicationCont.alamat.value.text),
          SizedBox(
            height: 8,
          ),
          Divider(),
          SizedBox(
            height: 8,
          ),
          AppText.appHeroText('Maklumat Simati'),
          SizedBox(
            height: AppSize.spaceX24,
          ),
          AppText.appTitleText('nama simati'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(
            applicationCont.namaSimati.value.text,
          ),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('no kad pengenalan'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(
            applicationCont.kadPengenalanSimati.value.text,
          ),
          SizedBox(
            height: AppSize.spaceX16,
          ),
          AppText.appTitleText('tarikh kematian'),
          SizedBox(
            height: 4,
          ),
          AppText.appSubtitleText(
            applicationCont.tarikhKematian.value.text,
          ),
        ],
      ),
    );
  }
}
