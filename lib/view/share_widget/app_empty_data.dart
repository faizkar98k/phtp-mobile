import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppEmptyData extends StatelessWidget {
  final String title;
  final String subttile;

  AppEmptyData({this.title, this.subttile});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSize.widthScreen(context),
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/folder.png',
            height: 80,
          ),
          Text(
            title ?? '',
            style: TextStyle(
              color: AppColors.darkTextColor,
              fontWeight: FontWeight.bold,
              fontSize: AppSize.fontLarge,
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            subttile ?? '',
            style: TextStyle(
              color: AppColors.labelTextColor,
            ),
          )
        ],
      ),
    );
  }
}
