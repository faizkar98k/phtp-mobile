import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppTextFieldWithLabel extends StatefulWidget {
  final String label;
  final TextInputAction textInputAction;
  final TextEditingController controller;
  final double width;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final String hint;
  final TextInputType keyboardType;
  final Widget icon;
  final Widget prefixIcon;
  final Function onTap;
  final bool readOnly;
  final ValueChanged<String> onSubmitted;
  final ValueChanged<String> validatorFunction;
  final ValueChanged<String> onSaveFunction;
  final ValueChanged<String> onChange;
  final FocusNode focusNode;
  final String errorText;
  final bool isPassword;
  final TextAlign textAlign;
  final bool obsecureText;
  final Color borderColor;
  final double borderWidth;
  final List<TextInputFormatter> inputFormatter;
  final bool enabled;
  final Function onTextFieldTap;
  final bool isChangePassword;
  final int maxLength;

  AppTextFieldWithLabel({
    this.label,
    this.textInputAction = TextInputAction.done,
    this.validatorFunction,
    this.onSaveFunction,
    this.onChange,
    this.focusNode,
    this.onSubmitted,
    this.controller,
    this.width = 300,
    this.margin = const EdgeInsets.symmetric(vertical: 4),
    this.padding = const EdgeInsets.symmetric(horizontal: 0),
    this.hint = "",
    this.keyboardType = TextInputType.text,
    this.icon,
    this.onTap,
    this.readOnly = false,
    this.obsecureText,
    this.textAlign = TextAlign.center,
    this.prefixIcon,
    this.errorText,
    this.isPassword = false,
    this.borderColor = Colors.grey,
    this.borderWidth = 1,
    this.inputFormatter,
    this.enabled = true,
    this.onTextFieldTap,
    this.isChangePassword = false,
    this.maxLength,
  });

  @override
  _AppTextFieldWithLabelState createState() => _AppTextFieldWithLabelState();
}

class _AppTextFieldWithLabelState extends State<AppTextFieldWithLabel> {
  bool show = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          this.widget.label ?? '',
          style: TextStyle(
            color: AppColors.labelTextColor,
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: AppSize.spaceX8,
        ),
        TextFormField(
          maxLength: this.widget.maxLength,
          onTap: this.widget.onTextFieldTap,
          obscureText: this.widget.isPassword ? show : false,
          enabled: this.widget.enabled,
          controller: this.widget.controller,
          onChanged: this.widget.onChange,
          onSaved: this.widget.onSaveFunction,
          onFieldSubmitted: this.widget.onSubmitted,
          validator: this.widget.validatorFunction,
          keyboardType: this.widget.keyboardType,
          style: TextStyle(
            color: this.widget.enabled
                ? AppColors.darkTextColor
                : AppColors.grayColor,
          ),
          decoration: InputDecoration(
            counterText: "",
            enabled: this.widget.enabled,
            contentPadding:
                EdgeInsets.symmetric(vertical: 18.0, horizontal: 16.0),
            floatingLabelBehavior: FloatingLabelBehavior.never,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.dangerColor,
                width: 1,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.dangerColor,
                width: 1.2,
              ),
            ),
            suffixIcon: this.widget.isChangePassword
                ? this.widget.isPassword
                    ? GestureDetector(
                        child: Icon(
                          show ? EvaIcons.eyeOutline : EvaIcons.eyeOffOutline,
                          color: AppColors.grayColor,
                        ),
                        onTap: () {
                          setState(() {
                            show = !show;
                          });
                        },
                      )
                    : null
                : null,
          ),
        ),
      ],
    );
  }
}
