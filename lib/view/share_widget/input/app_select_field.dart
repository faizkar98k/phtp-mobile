import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppSelectField extends StatefulWidget {
  final String label;
  final List items;
  final String value;
  final double width;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final String hint;
  final Function onTap;
  final bool readOnly;
  final ValueChanged<String> onSubmitted;
  final ValueChanged<String> validatorFunction;
  final ValueChanged<String> onSaveFunction;
  final ValueChanged<String> onChange;
  final FocusNode focusNode;
  final String errorText;
  final TextAlign textAlign;
  final bool obsecureText;
  final Color borderColor;
  final double borderWidth;
  final List<TextInputFormatter> inputFormatter;

  AppSelectField({
    this.value,
    this.items,
    this.label,
    this.validatorFunction,
    this.onSaveFunction,
    this.onChange,
    this.focusNode,
    this.onSubmitted,
    this.width = 300,
    this.margin = const EdgeInsets.symmetric(vertical: 4),
    this.padding = const EdgeInsets.symmetric(horizontal: 0),
    this.hint = "",
    this.onTap,
    this.readOnly = false,
    this.obsecureText,
    this.textAlign = TextAlign.center,
    this.errorText,
    this.borderColor = Colors.grey,
    this.borderWidth = 1,
    this.inputFormatter,
  });

  @override
  _AppTextFieldWithLabelState createState() => _AppTextFieldWithLabelState();
}

class _AppTextFieldWithLabelState extends State<AppSelectField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          this.widget.label ?? '',
          style: TextStyle(
            color: AppColors.labelTextColor,
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: AppSize.spaceX8,
        ),
        DropdownButtonFormField(
          isExpanded: true,
          style: TextStyle(),
          value: this.widget.value ?? '',
          items: this.widget.items.map<DropdownMenuItem<String>>((value) {
            return DropdownMenuItem(
              value: value,
              child: Text(
                value,
                style: TextStyle(
                  color: AppColors.darkTextColor,
                ),
              ),
            );
          }).toList(),
          onTap: () => FocusScope.of(context).unfocus(),
          onChanged: this.widget.onChange,
          onSaved: this.widget.onSaveFunction,
          validator: this.widget.validatorFunction,
          decoration: InputDecoration(
            hintText: this.widget.hint ?? '',
            contentPadding:
                EdgeInsets.symmetric(vertical: 15.0, horizontal: 16.0),
            floatingLabelBehavior: FloatingLabelBehavior.never,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.lightColor,
                width: 1,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.dangerColor,
                width: 1,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.dangerColor,
                width: 1.2,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
