import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/auth/login.dart';
import 'package:phtp_mobile/view/faq.dart';
import 'package:phtp_mobile/view/guide.dart';
import 'package:phtp_mobile/view/home.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';
import 'package:phtp_mobile/view/user_manual.dart';

class AppMainBar extends StatelessWidget {
  final Widget child;
  final bool hasAppBar;
  final Color backgroundColor;
  final String title;
  final bool centerTitle;
  final bool showLoading;
  final double elevation;
  final Color colorLoading;
  final Widget action;
  final bool hasBottomNavigation;
  final Widget bottomNavigationBar;
  final bool hasDrawer;
  final Function onBackFunction;

  AppMainBar({
    this.child,
    this.hasAppBar = true,
    this.backgroundColor = AppColors.primaryColor,
    this.title,
    this.showLoading = false,
    this.centerTitle = true,
    this.elevation,
    this.colorLoading,
    this.action,
    this.hasBottomNavigation = false,
    this.bottomNavigationBar,
    this.hasDrawer = false,
    this.onBackFunction,
  });

  final AccountController accountCont = Get.put(AccountController());

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Container(
      color: this.backgroundColor,
      child: SafeArea(
        child: Scaffold(
          drawer: hasDrawer
              ? Drawer(
                  child: Container(
                    padding: EdgeInsets.all(16),
                    color: AppColors.primaryColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.appTitleText('PILIHAN MENU'),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () =>
                              AppRoute.push(context, Home(selectedIndex: 0)),
                          child: appDrawerMenu(
                            'MyPermohonan',
                            Icons.home_filled,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () async {
                            AppRoute.pop(context);
                            AppRoute.push(context, Home(selectedIndex: 1));
                          },
                          child: appDrawerMenu(
                            'Meeting',
                            EvaIcons.video,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () =>
                              AppRoute.push(context, Home(selectedIndex: 2)),
                          child: appDrawerMenu(
                            'Profil',
                            EvaIcons.person,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () => AppRoute.push(context, UserManual()),
                          child: appDrawerMenu(
                            'Manual Pengguna',
                            EvaIcons.bookOpen,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () => AppRoute.push(context, Guide()),
                          child: appDrawerMenu(
                            'Panduan Permohonan',
                            EvaIcons.book,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () {
                            AppRoute.pop(context);
                            AppRoute.push(context, FAQ());
                          },
                          child: appDrawerMenu(
                            'Soalan Lazim',
                            EvaIcons.questionMarkCircle,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () async {
                            AppRoute.pop(context);
                            await accountCont.logout();
                            AppRoute.pushAndRemoveUntil(context, Login());
                          },
                          child: appDrawerMenu(
                            'Log Keluar',
                            EvaIcons.logOut,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : null,
          bottomNavigationBar:
              hasBottomNavigation ? this.bottomNavigationBar : null,
          appBar: this.hasAppBar
              ? AppBar(
                  title: Text(
                    this.title ?? '',
                    style: TextStyle(
                      fontSize: AppSize.fontLargeX2,
                    ),
                  ),
                  centerTitle: this.centerTitle,
                  backgroundColor: this.backgroundColor,
                  elevation: this.elevation ?? null,
                  actions: [this.action ?? Container()],
                  leading: this.hasDrawer
                      ? null
                      : InkWell(
                          onTap: this.onBackFunction ??
                              () => AppRoute.pop(context),
                          child: Icon(EvaIcons.arrowBack),
                        ),
                )
              : AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                ),
          body: this.child,
        ),
      ),
    );
  }
}

Widget appDrawerMenu(label, icon) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Icon(
        icon,
        color: Colors.white,
      ),
      SizedBox(
        width: 20,
      ),
      Text(
        label ?? '',
        style: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
    ],
  );
}
