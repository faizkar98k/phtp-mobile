import 'package:flutter/material.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppCheckList extends StatelessWidget {
  final String title;
  final bool isCompleted;
  final Function onTap;
  final int index;

  AppCheckList({
    this.title,
    this.isCompleted = false,
    this.onTap,
    this.index,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: AppColors.lightColor,
          shape: BoxShape.circle,
        ),
        child: Text(
          this.index.toString(),
          style: TextStyle(
            color: AppColors.darkTextColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      title: Text(
        title ?? '',
        style: TextStyle(color: AppColors.darkTextColor),
      ),
      trailing: Icon(
        EvaIcons.checkmarkCircle2,
        size: 28,
        color: isCompleted ? AppColors.successColor : AppColors.grayColor,
      ),
    );
  }
}
