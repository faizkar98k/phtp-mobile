import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class AppCacheImage extends StatelessWidget {
  final String imageUrl;
  AppCacheImage({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    if (imageUrl == null) {
      return Container(
        width: 54,
        height: 54,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          child: Icon(
            EvaIcons.image,
          ),
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            width: 54,
            height: 54,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => CircularProgressIndicator(
            strokeWidth: 4,
          ),
          errorWidget: (context, url, error) {
            return Container(
              width: width * 0.3,
              height: width * 0.3,
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(EvaIcons.people),
              ),
            );
          },
        ),
      );
    }
  }
}
