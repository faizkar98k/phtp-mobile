import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';

class Guide extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Panduan Permohonan',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 24,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Langkah 1 : Proses Pengisian Borang A',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              AppText.appSubtitleText(
                '1. Borang permohonan waris si mati (petisyen',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '2. Salinan sijil kematian simati',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '3. Salinan kad pengenalan bagi setiap waris yang berhak',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '4. 1 salinan surat beranak (bagi bukan islam)',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '5. 2 salinan CARIAN (RASMI/ PERSENDIRIAN)Geran Versi Terkini (RM 75.00)+ pelan',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '6. 1 salinan buku akaun bank/ Penyata KWSP/ASN dan lain-lain sekiranya ada',
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              Text(
                'Langkah 2 : Daftar Permohonan',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              AppText.appSubtitleText(
                '1. Borang”A” yang lengkap diisi beserta dokumen sokongan akan diserahkan kepada Unit pembahagian Pusaka kedah Tengah Aras 3, Zon A, Wisma Persekutuan Anak Bukit.',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '2. Pihak JKPTG (Unit Pembahagian Pusaka) akan menghantar surat akuan terima kepada pemohon/ pempetisyen dan memaklumkan tarikh jangkaan perbicaraan.',
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                'Langkah 3 : Perbicaraan',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              AppText.appSubtitleText(
                '1. Pemohon dikehendaki menunggu sehingga tarikh perbicaraan ditetapkan oleh pihak JKPTG (Unit Pembahagian Pusaka). Makluman adalah melalui Borang D/S – Notis Petisyen dan Pendengaran atau melalui panggilan telefon.',
              ),
              SizedBox(
                height: 4,
              ),
              AppText.appSubtitleText(
                '2. Semua waris dikehendaki hadir semasa perbicaraan dijalankan.',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
