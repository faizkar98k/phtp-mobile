import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:phtp_mobile/controller/application/benificiary_controller.dart';
import 'package:phtp_mobile/data/model/benificiary.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/app_button_upload_photo.dart';
import 'package:phtp_mobile/view/share_widget/app_cache_image.dart';
import 'package:phtp_mobile/view/share_widget/input/app_select_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class EditBenificiary extends StatefulWidget {
  @override
  _EditBenificiaryState createState() => _EditBenificiaryState();

  final Benificiary benificiary;
  EditBenificiary({this.benificiary});
}

class _EditBenificiaryState extends State<EditBenificiary> {
  GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  BenificiaryController benificiaryCont = Get.put(BenificiaryController());
  String imageUrl;
  final picker = ImagePicker();

  File file;

  imageFromCameraGallery(ImageSource imageSource) async {
    final pickedFile = await picker.getImage(source: imageSource);
    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
        imageUrl = file.path;
      }
    });
    AppRoute.pop(context);
  }

  openPhotoOptions() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.0),
              topLeft: Radius.circular(20.0),
            ),
          ),
          padding: EdgeInsets.all(24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //take photo
              // buttonUploadPhoto(
              //   'Take Photo',
              //   Icons.camera_alt,
              //   Colors.blueAccent,
              //   () {
              //     imageFromCameraGallery(ImageSource.camera);
              //   },
              // ),
              //gallery
              buttonUploadPhoto(
                'Gallery',
                Icons.photo_library,
                Colors.teal,
                () {
                  imageFromCameraGallery(ImageSource.gallery);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    benificiaryCont.hubungan(widget.benificiary.hubunganWaris);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Kemaskini Waris',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppSize.spaceX16),
          child: Obx(
            () => Form(
              key: formKey3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.name.value
                      ..text = widget.benificiary.namaWaris,
                    label: 'Nama Waris',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'Nama Waris tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    maxLength: 12,
                    controller: benificiaryCont.kadPengenalan.value
                      ..text = widget.benificiary.kadPengenalanWaris,
                    keyboardType: TextInputType.number,
                    label: 'No Kad Pengenalan',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No Kad Pengenalan tidak boleh kosong';
                      } else if (value.length < 12) {
                        return 'No Kad Pengenalan tidak boleh sah';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppSelectField(
                    value: widget.benificiary.hubunganWaris,
                    items: relative,
                    label: 'Hubungan Dengan Simati',
                    onChange: (value) {
                      benificiaryCont.hubungan(value);
                    },
                    hint: 'Pilih hubungan dengan simati',
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.alamat.value
                      ..text = widget.benificiary.alamatWaris,
                    label: 'Alamat',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'Alamat tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.telefon.value
                      ..text = widget.benificiary.telefon,
                    label: 'No. Telefon',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No. telefon tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  Text(
                    'Muat Naik Gambar Kad Pengenalan',
                    style: TextStyle(
                      color: AppColors.labelTextColor,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      FlatButton.icon(
                        icon: Icon(
                          Icons.upload_rounded,
                          color: AppColors.whiteColor,
                        ),
                        height: 54,
                        minWidth: 100,
                        color: AppColors.primaryColor,
                        onPressed: openPhotoOptions,
                        label: Text(
                          'Upload',
                          style: TextStyle(
                            color: AppColors.whiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      file != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.file(
                                file,
                                fit: BoxFit.cover,
                                width: 54,
                                height: 54,
                              ),
                            )
                          : AppCacheImage(
                              imageUrl: imageEndpoint +
                                  widget.benificiary.attachmentImgStoredPath,
                            ),
                    ],
                  ),
                  SizedBox(
                    height: AppSize.spaceX24,
                  ),
                  AppButton(
                    onTap: () async {
                      if (formKey3.currentState.validate()) {
                        await benificiaryCont.updateBenificiary(
                          file,
                          widget.benificiary.id,
                        );
                        AppRoute.pop(context);
                      }
                    },
                    btnTitle: 'Kemaskini',
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
