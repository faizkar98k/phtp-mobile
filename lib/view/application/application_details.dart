import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/view/application/applications.dart';
import 'package:phtp_mobile/view/application/e_borang_pdf.dart';
import 'package:phtp_mobile/view/checklist.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_cache_image.dart';
import 'package:phtp_mobile/view/share_widget/app_text.dart';

class ApplicationDetails extends StatefulWidget {
  @override
  _ApplicationDetailsState createState() => _ApplicationDetailsState();

  final Application application;
  ApplicationDetails({this.application});
}

class _ApplicationDetailsState extends State<ApplicationDetails> {
  ApplicationController applicationCont = ApplicationController();

  onTapFunction() async {
    await applicationCont.deleteApplication(widget.application.id);
    AppRoute.pushAndRemoveUntil(context, Applications());
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Butiran Permohonan',
      action: InkWell(
        onTap: () => showMaterialModalBottomSheet(
          expand: false,
          context: context,
          backgroundColor: Colors.transparent,
          builder: (context) {
            return Container(
              height: 150,
              color: Colors.white,
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () => AppRoute.push(
                      context,
                      EBorangPDF(
                        application: widget.application,
                      ),
                    ),
                    child: Container(
                      width: AppSize.widthScreen(context),
                      padding: EdgeInsets.all(16.0),
                      child: Center(
                        child: Text('Print'),
                      ),
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: AppColors.lightColor,
                  ),
                  GestureDetector(
                    onTap: () => AppRoute.push(
                      context,
                      CheckList(
                        application: widget.application,
                      ),
                    ),
                    child: Container(
                      width: AppSize.widthScreen(context),
                      padding: EdgeInsets.all(16.0),
                      child: Center(
                        child: Text('Senarai Semak'),
                      ),
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: AppColors.lightColor,
                  ),
                  GestureDetector(
                    onTap: () async {
                      await AppModal.showConfirmation(
                        context,
                        'Padam Permohonan',
                        'Adakah anda pasti ingin padam permohonan ini daripada senarai ',
                        onTapFunction,
                      );
                    },
                    child: Container(
                      width: AppSize.widthScreen(context),
                      padding: EdgeInsets.all(16.0),
                      child: Center(
                        child: Text(
                          'Delete',
                          style: TextStyle(
                            color: AppColors.dangerColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Icon(
            EvaIcons.moreVertical,
            color: AppColors.whiteColor,
          ),
        ),
      ),
      child: SingleChildScrollView(
        child: Container(
          width: AppSize.widthScreen(context),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              widget.application.status == 'pembicaraan'
                  ? Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: AppSize.spaceX16,
                        vertical: AppSize.spaceX16,
                      ),
                      width: AppSize.widthScreen(context),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.appHeroText('Maklumat Pembicaraan'),
                          SizedBox(
                            height: AppSize.spaceX24,
                          ),
                          AppText.appTitleText('Tempat Pembicaraan'),
                          SizedBox(
                            height: 4,
                          ),
                          AppText.appSubtitleText(
                            widget.application.tempatPembicaraan,
                          ),
                          SizedBox(
                            height: AppSize.spaceX16,
                          ),
                          AppText.appTitleText('tarikh pembicaraan'),
                          SizedBox(
                            height: 4,
                          ),
                          AppText.appSubtitleText(
                            DateFormat("dd-MM-yyyy").format(
                              DateTime.parse(
                                widget.application.pembicaraanDate,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: AppSize.spaceX16,
                          ),
                          AppText.appTitleText('masa pembicaraan'),
                          SizedBox(
                            height: 4,
                          ),
                          AppText.appSubtitleText(
                            DateFormat.jm().format(
                              DateTime.parse(
                                "2021-05-19 " +
                                    widget.application.pembicaraanTime +
                                    ":00.9123",
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            '* Pastikan semua ahli yang terlibat dapat hadir semasa proses pembicaraan.',
                            style: TextStyle(
                              color: AppColors.darkTextColor,
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: AppSize.spaceX16,
                  vertical: AppSize.spaceX16,
                ),
                width: AppSize.widthScreen(context),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText.appHeroText('Maklumat Pemohon'),
                    SizedBox(
                      height: AppSize.spaceX24,
                    ),
                    AppText.appTitleText('nama pemohon'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(widget.application.namaPemohon),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('no kad pengenalan'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(
                      widget.application.kadPengenalanPemohon,
                    ),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('alamat'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(
                      widget.application.kadPengenalanPemohon,
                    ),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('hubungan dengan simati'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(
                      widget.application.hubunganPemohon,
                    ),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('Salinan Kad Pengenalan'),
                    SizedBox(
                      height: 4,
                    ),
                    AppCacheImage(
                      imageUrl: imageEndpoint +
                          widget.application.attachmentImgStoredPathPemohon,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: AppSize.spaceX16,
                  vertical: AppSize.spaceX16,
                ),
                width: AppSize.widthScreen(context),
                color: AppColors.lightColor.withOpacity(0.5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText.appHeroText('Maklumat Simati'),
                    SizedBox(
                      height: AppSize.spaceX24,
                    ),
                    AppText.appTitleText('nama simati'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(widget.application.namaSimati),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('no kad pengenalan'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(
                      widget.application.kadPengenalanSimati,
                    ),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('tarikh kematian'),
                    SizedBox(
                      height: 4,
                    ),
                    AppText.appSubtitleText(widget.application.tarikhKematian),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('Salinan Kad Pengenalan'),
                    SizedBox(
                      height: 4,
                    ),
                    AppCacheImage(
                      imageUrl: imageEndpoint +
                          widget.application.attachmentImgStoredPathSimati,
                    ),
                    SizedBox(
                      height: AppSize.spaceX16,
                    ),
                    AppText.appTitleText('Salinan Sijil Kematian'),
                    SizedBox(
                      height: 4,
                    ),
                    AppCacheImage(
                      imageUrl: imageEndpoint +
                          widget.application.attachmentImgStoredPathSijil,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: AppSize.spaceX16,
                  vertical: AppSize.spaceX16,
                ),
                width: AppSize.widthScreen(context),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText.appHeroText('Maklumat Waris'),
                    SizedBox(
                      height: AppSize.spaceX24,
                    ),
                    ...widget.application.benificiary.map(
                      (v) {
                        int index = widget.application.benificiary.indexOf(v);
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            appBadge('waris ${index + 1}'),
                            SizedBox(
                              height: 8,
                            ),
                            AppText.appTitleText('nama waris'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.namaWaris),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('no kad pengenalan'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.kadPengenalanWaris),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('alamat'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.alamatWaris),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('hubungan dengan simati'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.hubunganWaris),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('Salinan Kad Pengenalan'),
                            SizedBox(
                              height: 4,
                            ),
                            AppCacheImage(
                              imageUrl:
                                  imageEndpoint + v.attachmentImgStoredPath,
                            ),
                          ],
                        );
                      },
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: AppSize.spaceX16,
                  vertical: AppSize.spaceX16,
                ),
                width: AppSize.widthScreen(context),
                color: AppColors.lightColor.withOpacity(0.5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText.appHeroText('Maklumat Harta Tak Alih'),
                    SizedBox(
                      height: AppSize.spaceX24,
                    ),
                    ...widget.application.inheritance.map(
                      (v) {
                        int index = widget.application.inheritance.indexOf(v);
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            appBadge('harta ${index + 1}'),
                            SizedBox(
                              height: 8,
                            ),
                            AppText.appTitleText('no hak milik'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.noHakMilik),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('no lot'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.noLot),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('alamat harta'),
                            SizedBox(
                              height: 4,
                            ),
                            AppText.appSubtitleText(v.alamat),
                            SizedBox(
                              height: AppSize.spaceX16,
                            ),
                            AppText.appTitleText('Salinan Geran Hak Milik'),
                            SizedBox(
                              height: 4,
                            ),
                            AppCacheImage(
                              imageUrl:
                                  imageEndpoint + v.attachmentImgStoredPath,
                            ),
                          ],
                        );
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget appBadge(String label) {
  return Container(
    padding: EdgeInsets.symmetric(
      vertical: 4,
      horizontal: 8,
    ),
    decoration: BoxDecoration(
      color: AppColors.darkBlueColor,
      borderRadius: BorderRadius.circular(6.0),
    ),
    child: Text(
      label.toUpperCase() ?? '',
      style: TextStyle(
        color: AppColors.whiteColor,
      ),
    ),
  );
}
