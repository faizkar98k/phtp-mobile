import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/application/inheritance_controller.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/application/create_inheritance.dart';
import 'package:phtp_mobile/view/application/edit_inheritance.dart';
import 'package:phtp_mobile/view/share_widget/card/app_inheritance_card.dart';

class InheritanceDetails extends StatefulWidget {
  @override
  _InheritanceDetailsState createState() => _InheritanceDetailsState();
}

class _InheritanceDetailsState extends State<InheritanceDetails> {
  InheritanceController inheritanceCont = Get.put(InheritanceController());

  @override
  void initState() {
    getInheritance();
    super.initState();
  }

  getInheritance() async {
    await inheritanceCont.getInheritance();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => AppRoute.push(context, CreateInheritance()),
          child: Container(
            width: AppSize.maxWidthScreen(context),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: AppColors.lightColor,
                  child: Icon(
                    EvaIcons.personAddOutline,
                    color: AppColors.primaryColor,
                  ),
                ),
                SizedBox(
                  width: AppSize.spaceX16,
                ),
                Text(
                  'Tambah Harta Tak Alih',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: AppColors.labelTextColor,
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: AppSize.spaceX16,
        ),
        Divider(
          height: 1,
          color: AppColors.lightColor,
        ),
        Obx(
          () => ListView.builder(
            physics: ScrollPhysics(),
            itemCount: inheritanceCont.inheritanceList.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => AppRoute.push(
                  context,
                  EditInheritance(
                    inheritance: inheritanceCont.inheritanceList[index],
                  ),
                ),
                child: AppInheritanceCard(
                  title:
                      inheritanceCont.inheritanceList[index].noHakMilik ?? '',
                  subtitle1: inheritanceCont.inheritanceList[index].noLot ?? '',
                  subtitle2:
                      inheritanceCont.inheritanceList[index].alamat ?? '',
                  onDelete: () async => await inheritanceCont.deleteInheritance(
                    inheritanceCont.inheritanceList[index].id,
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
