import 'package:flutter/material.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/application/benificiary_controller.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/application/create_benificiary.dart';
import 'package:phtp_mobile/view/application/edit_benificiary.dart';
import 'package:phtp_mobile/view/share_widget/card/app_benficiary_card.dart';

class BenificiaryDetails extends StatefulWidget {
  @override
  _BenificiaryDetailsState createState() => _BenificiaryDetailsState();

  final Application application;
  BenificiaryDetails({this.application});
}

class _BenificiaryDetailsState extends State<BenificiaryDetails> {
  BenificiaryController benificiaryCont = Get.put(BenificiaryController());

  @override
  void initState() {
    getBenificiary();
    super.initState();
  }

  getBenificiary() async {
    await benificiaryCont.getBenificiary();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => AppRoute.push(context, CreateBenificiary()),
          child: Container(
            width: AppSize.maxWidthScreen(context),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: AppColors.lightColor,
                  child: Icon(
                    EvaIcons.personAddOutline,
                    color: AppColors.primaryColor,
                  ),
                ),
                SizedBox(
                  width: AppSize.spaceX16,
                ),
                Text(
                  'Tambah Waris',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: AppColors.labelTextColor,
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: AppSize.spaceX16,
        ),
        Divider(
          height: 1,
          color: AppColors.lightColor,
        ),
        Obx(
          () => ListView.builder(
            physics: ScrollPhysics(),
            itemCount: benificiaryCont.benificiaryList.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => AppRoute.push(
                  context,
                  EditBenificiary(
                    benificiary: benificiaryCont.benificiaryList[index],
                  ),
                ),
                child: AppBenificiaryCard(
                  onDelete: () async {
                    await benificiaryCont.deleteBenificiary(
                      benificiaryCont.benificiaryList[index].id,
                    );
                  },
                  title: benificiaryCont.benificiaryList[index].namaWaris,
                  subtitle1:
                      benificiaryCont.benificiaryList[index].kadPengenalanWaris,
                  subtitle2:
                      benificiaryCont.benificiaryList[index].hubunganWaris,
                  subtitle3: benificiaryCont.benificiaryList[index].alamatWaris,
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
