import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:phtp_mobile/controller/application/benificiary_controller.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/app_button_upload_photo.dart';
import 'package:phtp_mobile/view/share_widget/input/app_select_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class CreateBenificiary extends StatefulWidget {
  @override
  _CreateBenificiaryState createState() => _CreateBenificiaryState();
}

class _CreateBenificiaryState extends State<CreateBenificiary> {
  GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  BenificiaryController benificiaryCont = Get.put(BenificiaryController());
  String imageUrl;
  final picker = ImagePicker();

  File file;

  imageFromCameraGallery(ImageSource imageSource) async {
    final pickedFile = await picker.getImage(source: imageSource);
    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
        imageUrl = file.path;
      }
    });
    AppRoute.pop(context);
  }

  @override
  void initState() {
    benificiaryCont.alamat.value.text = '';
    benificiaryCont.name.value.text = '';
    benificiaryCont.telefon.value.text = '';
    benificiaryCont.hubungan.value = 'Tiada Maklumat';
    benificiaryCont.kadPengenalan.value.text = '';
    super.initState();
  }

  openPhotoOptions() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.0),
              topLeft: Radius.circular(20.0),
            ),
          ),
          padding: EdgeInsets.all(24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //take photo
              // buttonUploadPhoto(
              //   'Take Photo',
              //   Icons.camera_alt,
              //   Colors.blueAccent,
              //   () {
              //     imageFromCameraGallery(ImageSource.camera);
              //   },
              // ),
              //gallery
              buttonUploadPhoto(
                'Gallery',
                Icons.photo_library,
                Colors.teal,
                () {
                  imageFromCameraGallery(ImageSource.gallery);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Tambah Waris',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppSize.spaceX16),
          child: Obx(
            () => Form(
              key: formKey3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.name.value,
                    label: 'Nama Waris',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'Nama Waris tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.kadPengenalan.value,
                    keyboardType: TextInputType.number,
                    maxLength: 12,
                    label: 'No Kad Pengenalan',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No Kad Pengenalan tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppSelectField(
                    value: benificiaryCont.hubungan.value,
                    items: relative,
                    label: 'Hubungan Dengan Simati',
                    onChange: (value) {
                      benificiaryCont.hubungan(value);
                    },
                    hint: 'Pilih hubungan dengan simati',
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.alamat.value,
                    label: 'Alamat',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'Alamat tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    controller: benificiaryCont.telefon.value,
                    label: 'No. Telefon',
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No. telefon tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  Text(
                    'Muat Naik Gambar Kad Pengenalan',
                    style: TextStyle(
                      color: AppColors.labelTextColor,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      FlatButton.icon(
                        icon: Icon(
                          Icons.upload_rounded,
                          color: AppColors.whiteColor,
                        ),
                        height: 54,
                        minWidth: 100,
                        color: AppColors.primaryColor,
                        onPressed: openPhotoOptions,
                        label: Text(
                          'Upload',
                          style: TextStyle(
                            color: AppColors.whiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      file != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.file(
                                file,
                                fit: BoxFit.cover,
                                width: 54,
                                height: 54,
                              ),
                            )
                          : Container(),
                    ],
                  ),
                  SizedBox(
                    height: AppSize.spaceX24,
                  ),
                  AppButton(
                    onTap: () async {
                      if (formKey3.currentState.validate() && file != null) {
                        await benificiaryCont.createBenificiary(file);
                        AppRoute.pop(context);
                      }
                      if (file == null) {
                        return AppModal.showInformation(
                          context,
                          'Error',
                          'Anda perlu muat naik gambar kad pengenalan waris.',
                        );
                      }
                    },
                    btnTitle: 'Simpan',
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
