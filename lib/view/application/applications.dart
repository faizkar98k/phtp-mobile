import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/controller/application/benificiary_controller.dart';
import 'package:phtp_mobile/controller/application/inheritance_controller.dart';
import 'package:phtp_mobile/data/cache/app_storage.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/utils/app_parse_date.dart';
import 'package:phtp_mobile/utils/app_refresher.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/view/application/add_application_details.dart';
import 'package:phtp_mobile/view/application/application_details.dart';
import 'package:phtp_mobile/view/home.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_empty_data.dart';
import 'package:phtp_mobile/view/share_widget/app_loading.dart';
import 'package:phtp_mobile/view/share_widget/card/app_application_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Applications extends StatefulWidget {
  @override
  _ApplicationsState createState() => _ApplicationsState();
}

class _ApplicationsState extends State<Applications> {
  ApplicationController applicationCont = Get.put(ApplicationController());
  RefreshController refreshController = RefreshController();
  BenificiaryController benificiaryCont = Get.put(BenificiaryController());
  InheritanceController inheritanceCont = Get.put(InheritanceController());

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await applicationCont.getAllApplication();
    });
    initializeDateFormatting();
    super.initState();
  }

  onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    await applicationCont.getAllApplication();
    refreshController.refreshCompleted();
  }

  onTap(index) async {
    Application application = applicationCont.applicationList[index];
    AppStorage.storage.write('application_id', application.id);

    await benificiaryCont.getBenificiary();
    await inheritanceCont.getInheritance();

    if (application.status == "tidak lengkap" ||
        benificiaryCont.benificiaryList.isEmpty ||
        inheritanceCont.inheritanceList.isEmpty) {
      AppStorage.storage.write('application_id', application.id);
      AppRoute.push(context, AddApplicationDetails());
    } else {
      AppRoute.push(context, ApplicationDetails(application: application));
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Draf Permohonan',
      onBackFunction: () => AppRoute.pushAndRemoveUntil(
        context,
        Home(selectedIndex: 0),
      ),
      child: AppRefresher(
        controller: refreshController,
        onRefresh: onRefresh,
        enablePullDown: true,
        enablePullUp: false,
        child: Obx(
          () => applicationCont.loading.value
              ? AppLoading()
              : applicationCont.applicationList.isEmpty
                  ? AppEmptyData(
                      title: 'Tiada Permohonan',
                      subttile: 'Anda tidak pernah membuat permohonan lagi.',
                    )
                  : AppRefresher(
                      controller: refreshController,
                      onRefresh: onRefresh,
                      enablePullDown: true,
                      enablePullUp: false,
                      child: ListView.builder(
                        itemCount: applicationCont.applicationList.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () => onTap(index),
                            child: AppApplicationCard(
                              title: '#' +
                                      applicationCont
                                          .applicationList[index].referenceId ??
                                  '',
                              subtitle1: applicationCont
                                      .applicationList[index].namaPemohon ??
                                  '',
                              subtitle2: AppParseDate.getDateTime(
                                    applicationCont
                                        .applicationList[index].createdAt,
                                  ) ??
                                  '',
                              tempatBicara: applicationCont
                                      .applicationList[index]
                                      .tempatPembicaraan ??
                                  " ",
                              timeBicara: applicationCont
                                      .applicationList[index].pembicaraanTime ??
                                  " ",
                              dateBicara: applicationCont
                                      .applicationList[index].pembicaraanDate ??
                                  " ",
                              status: applicationCont
                                      .applicationList[index].status ??
                                  '',
                              isSuccess: applicationCont
                                      .applicationList[index].status ==
                                  'lengkap',
                              onDelete: () async =>
                                  await applicationCont.deleteApplication(
                                applicationCont.applicationList[index].id,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
        ),
      ),
    );
  }
}
