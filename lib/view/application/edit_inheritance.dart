import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:phtp_mobile/controller/application/inheritance_controller.dart';
import 'package:phtp_mobile/data/model/inheritance.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/app_urls.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/app_button_upload_photo.dart';
import 'package:phtp_mobile/view/share_widget/app_cache_image.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class EditInheritance extends StatefulWidget {
  @override
  _EditInheritanceState createState() => _EditInheritanceState();

  final Inheritance inheritance;
  EditInheritance({this.inheritance});
}

class _EditInheritanceState extends State<EditInheritance> {
  GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  InheritanceController inheritanceCont = Get.put(InheritanceController());

  String imageUrl;
  final picker = ImagePicker();

  File file;

  imageFromCameraGallery(ImageSource imageSource) async {
    final pickedFile = await picker.getImage(source: imageSource);
    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
        imageUrl = file.path;
      }
    });
    AppRoute.pop(context);
  }

  openPhotoOptions() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.0),
              topLeft: Radius.circular(20.0),
            ),
          ),
          padding: EdgeInsets.all(24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //take photo
              // buttonUploadPhoto(
              //   'Take Photo',
              //   Icons.camera_alt,
              //   Colors.blueAccent,
              //   () {
              //     imageFromCameraGallery(ImageSource.camera);
              //   },
              // ),
              //gallery
              buttonUploadPhoto(
                'Gallery',
                Icons.photo_library,
                Colors.teal,
                () {
                  imageFromCameraGallery(ImageSource.gallery);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Kemaskini Harta Tak Alih',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppSize.spaceX16),
          child: Obx(
            () => Form(
              key: formKey4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextFieldWithLabel(
                    label: 'No. Hak Milik',
                    controller: inheritanceCont.noHakMilik.value
                      ..text = widget.inheritance.noHakMilik,
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No Hak Milik tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    label: 'No. Lot',
                    controller: inheritanceCont.noLot.value
                      ..text = widget.inheritance.noLot,
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'No Lot tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  AppTextFieldWithLabel(
                    label: 'Alamat Harta',
                    controller: inheritanceCont.alamat.value
                      ..text = widget.inheritance.alamat,
                    validatorFunction: (value) {
                      if (value.isEmpty) {
                        return 'Alamat harta tidak boleh kosong';
                      }
                    },
                  ),
                  SizedBox(
                    height: AppSize.spaceX16,
                  ),
                  Text(
                    'Muat Naik Gambar Geran',
                    style: TextStyle(
                      color: AppColors.labelTextColor,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      FlatButton.icon(
                        icon: Icon(
                          Icons.upload_rounded,
                          color: AppColors.whiteColor,
                        ),
                        height: 54,
                        minWidth: 100,
                        color: AppColors.primaryColor,
                        onPressed: openPhotoOptions,
                        label: Text(
                          'Upload',
                          style: TextStyle(
                            color: AppColors.whiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      file != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.file(
                                file,
                                fit: BoxFit.cover,
                                width: 54,
                                height: 54,
                              ),
                            )
                          : AppCacheImage(
                              imageUrl: imageEndpoint +
                                  widget.inheritance.attachmentImgStoredPath,
                            ),
                    ],
                  ),
                  SizedBox(
                    height: AppSize.spaceX24,
                  ),
                  AppButton(
                    onTap: () async {
                      if (formKey4.currentState.validate()) {
                        await inheritanceCont.updateInheritance(
                          file,
                          widget.inheritance.id,
                        );
                        AppRoute.pop(context);
                      }
                    },
                    btnTitle: 'Kemaskini',
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
