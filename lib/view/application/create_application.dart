import 'dart:io';
import 'package:cool_stepper/cool_stepper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/utils/app_config.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/application/add_application_details.dart';
import 'package:phtp_mobile/view/share_widget/app_application_review.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button_upload_photo.dart';
import 'package:phtp_mobile/view/share_widget/input/app_date_time_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_select_field.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class CreateApplication extends StatefulWidget {
  @override
  _CreateApplicationState createState() => _CreateApplicationState();
}

class _CreateApplicationState extends State<CreateApplication> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  ApplicationController applicationCont = Get.put(ApplicationController());
  String imageUrl;
  final picker = ImagePicker();
  File file1;
  File file2;
  File file3;

  imageFromCameraGallery(ImageSource imageSource, int type) async {
    final pickedFile = await picker.getImage(source: imageSource);
    setState(() {
      if (type == 1) {
        if (pickedFile != null) {
          file1 = File(pickedFile.path);
          imageUrl = file1.path;
        }
      } else if (type == 2) {
        if (pickedFile != null) {
          file2 = File(pickedFile.path);
          imageUrl = file2.path;
        }
      } else if (type == 3) {
        if (pickedFile != null) {
          file3 = File(pickedFile.path);
          imageUrl = file3.path;
        }
      }
    });
    AppRoute.pop(context);
  }

  openPhotoOptions(int type) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.0),
              topLeft: Radius.circular(20.0),
            ),
          ),
          padding: EdgeInsets.all(24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //take photo
              // buttonUploadPhoto(
              //   'Take Photo',
              //   Icons.camera_alt,
              //   Colors.blueAccent,
              //   () {
              //     imageFromCameraGallery(ImageSource.camera, type);
              //   },
              // ),
              //gallery
              buttonUploadPhoto(
                'Gallery',
                Icons.photo_library,
                Colors.teal,
                () {
                  imageFromCameraGallery(ImageSource.gallery, type);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<CoolStep> steps = [
      CoolStep(
        title: "Maklumat Pemohon",
        subtitle: "Sila lengkapkan maklumat pemohon untuk teruskan.",
        content: Form(
          key: formKey,
          child: Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppSelectField(
                  value: applicationCont.tarafKepentingan.value,
                  items: taraf,
                  label: 'Taraf Kepentingan',
                  onChange: (value) {
                    applicationCont.tarafKepentingan(value);
                  },
                  hint: 'Pilih taraf kepentingan',
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: applicationCont.namaPemohon.value,
                  label: 'Nama Pemohon',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Nama Pemohon tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  maxLength: 12,
                  controller: applicationCont.kadPengenalanPemohon.value,
                  label: 'No Kad Pengenalan',
                  keyboardType: TextInputType.number,
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'No Kad Pengenalan tidak boleh kosong';
                    } else if (value.length < 12) {
                      return 'No kad pengenalan tidak sah';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppSelectField(
                  value: applicationCont.hubunganPemohon.value,
                  items: relative,
                  label: 'Hubungan Dengan Simati',
                  onChange: (value) {
                    applicationCont.hubunganPemohon(value);
                  },
                  hint: 'Pilih hubungan dengan simati',
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: applicationCont.telefon.value,
                  keyboardType: TextInputType.number,
                  label: 'No. Telefon',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'No Telefon tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: applicationCont.alamat.value,
                  label: 'Alamat',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Alamat tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                Text(
                  'Muat Naik Gambar Kad Pengenalan',
                  style: TextStyle(
                    color: AppColors.labelTextColor,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    FlatButton.icon(
                      icon: Icon(
                        Icons.upload_rounded,
                        color: AppColors.whiteColor,
                      ),
                      height: 54,
                      minWidth: 100,
                      color: AppColors.primaryColor,
                      onPressed: () => openPhotoOptions(1),
                      label: Text(
                        'Upload',
                        style: TextStyle(
                          color: AppColors.whiteColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    file1 != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.file(
                              file1,
                              fit: BoxFit.cover,
                              width: 54,
                              height: 54,
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  width: 10.0,
                ),
              ],
            ),
          ),
        ),
        validation: () {
          if (!formKey.currentState.validate()) {
            return "Fill form correctly";
          } else if (file1 == null) {
            return AppModal.showInformation(
              context,
              'Error',
              'Anda perlu muat naik salinan gambar pengenalan.',
            );
          } else {
            return null;
          }
        },
      ),
      CoolStep(
        title: "Maklumat Simati",
        subtitle:
            "Sila lengkapkan maklumat simati untuk melengkapkan permohonan anda.",
        content: Form(
          key: formKey2,
          child: Obx(
            () => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextFieldWithLabel(
                  controller: applicationCont.namaSimati.value,
                  label: 'Nama Simati',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Nama Simati tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  maxLength: 12,
                  controller: applicationCont.kadPengenalanSimati.value,
                  label: 'No Kad Pengenalan',
                  keyboardType: TextInputType.number,
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'No kad pengenalan tidak boleh kosong';
                    } else if (value.length < 12) {
                      return 'No kad pengenalan tidak sah';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppDateTimeField(
                  controller: applicationCont.tarikhKematian.value,
                  isApplication: true,
                  label: 'Tarikh Kematian',
                  // onChange: (value) => applicationCont
                  //     .tarikhKematian.value.text = value.toString(),
                  validatorFunction: (value) {
                    if (value == null) {
                      return 'Tarikh Kematian tidak boleh kosong';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                Text(
                  'Muat Naik Gambar Kad Pengenalan',
                  style: TextStyle(
                    color: AppColors.labelTextColor,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    FlatButton.icon(
                      icon: Icon(
                        Icons.upload_rounded,
                        color: AppColors.whiteColor,
                      ),
                      height: 54,
                      minWidth: 100,
                      color: AppColors.primaryColor,
                      onPressed: () => openPhotoOptions(2),
                      label: Text(
                        'Upload',
                        style: TextStyle(
                          color: AppColors.whiteColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    file2 != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.file(
                              file2,
                              fit: BoxFit.cover,
                              width: 54,
                              height: 54,
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                Text(
                  'Muat Naik Gambar Sijil Kematian',
                  style: TextStyle(
                    color: AppColors.labelTextColor,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    FlatButton.icon(
                      icon: Icon(
                        Icons.upload_rounded,
                        color: AppColors.whiteColor,
                      ),
                      height: 54,
                      minWidth: 100,
                      color: AppColors.primaryColor,
                      onPressed: () => openPhotoOptions(3),
                      label: Text(
                        'Upload',
                        style: TextStyle(
                          color: AppColors.whiteColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    file3 != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.file(
                              file3,
                              fit: BoxFit.cover,
                              width: 54,
                              height: 54,
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
              ],
            ),
          ),
        ),
        validation: () {
          if (!formKey2.currentState.validate()) {
            return "Fill form correctly";
          } else if (file2 == null) {
            return AppModal.showInformation(
              context,
              'Error',
              'Anda perlu muat naik salinan gambar kad pengenalan si mati',
            );
          } else if (file3 == null) {
            return AppModal.showInformation(
              context,
              'Error',
              'Anda perlu muat naik salinan gambar sijil kematian si mati',
            );
          }
          return null;
        },
      ),
      CoolStep(
        title: "Maklumat Pemohon dan Simati",
        subtitle: "Sila pastikan maklumat yang diisi adalah betul.",
        content: Container(
          width: AppSize.widthScreen(context),
          child: AppApplicationReview(),
        ),
        validation: () => null,
      ),
    ];
    return AppMainBar(
      elevation: 0.0,
      title: 'Permohonan Baru',
      child: Container(
        child: CoolStepper(
          onCompleted: () async {
            AppModal.showConfirmation(
              context,
              'Simpan Maklumat',
              'Adakah anda pasti ingin menyimpan permohonan yang diisi?',
              () async {
                await applicationCont.createApplication(file1, file2, file3);
                AppRoute.pushReplacement(context, AddApplicationDetails());
              },
            );
          },
          steps: steps,
          config: coolStepperConfig,
        ),
      ),
    );
  }
}
