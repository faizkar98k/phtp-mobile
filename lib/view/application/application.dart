import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/card/app_card.dart';

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  AccountController accountController = Get.put(AccountController());
  ApplicationController applicationCont = Get.put(ApplicationController());

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await applicationCont.getAllApplication();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: AppSize.spaceX16,
        right: AppSize.spaceX16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: AppSize.spaceX24,
          ),
          Obx(
            () => Text(
              'Selamat datang, ${accountController.user.value.name ?? ""}',
              style: TextStyle(
                color: AppColors.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: AppSize.fontLargeX2,
              ),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            'Lakukan permohonan anda sekarang.',
            style: TextStyle(
              color: AppColors.labelTextColor,
              fontSize: AppSize.fontMedium,
            ),
          ),
          SizedBox(
            height: AppSize.spaceX48,
          ),
          Expanded(
            child: StaggeredGridView.countBuilder(
              shrinkWrap: true,
              crossAxisCount: 4,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) {
                return Obx(
                  () => AppCard(
                    title: applicationCard[index]['title'],
                    subtitle: applicationCard[index]['subtitle'],
                    hasTotal: index == 1 ?? false,
                    total: applicationCont.applicationList.length.toString(),
                    onTap: () => AppRoute.push(
                      context,
                      applicationCard[index]['page'],
                    ),
                  ),
                );
              },
              staggeredTileBuilder: (int index) =>
                  StaggeredTile.count(2, index.isEven ? 2 : 2.5),
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
            ),
          )
        ],
      ),
    );
  }
}
