import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:printing/printing.dart';

class EBorangPDF extends StatefulWidget {
  @override
  _EBorangPDFState createState() => _EBorangPDFState();

  final Application application;
  EBorangPDF({this.application});
}

class _EBorangPDFState extends State<EBorangPDF> {
  ApplicationController applicationCont = Get.put(ApplicationController());

  @override
  void initState() {
    applicationCont.getApplication();
    super.initState();
  }

  final pdf = pw.Document();

  @override
  Widget build(BuildContext context) {
    Future<Uint8List> pdfFunction() async {
      pdf.addPage(
        pw.MultiPage(
          pageFormat: PdfPageFormat.a4,
          build: (pw.Context context) {
            return [
              pw.Center(
                child: pw.Text('Borang A'),
              ),
              pw.Center(child: pw.Text('[Subperaturan 3(1)]')),
              pw.SizedBox(height: 10.0),
              pw.Center(
                child: pw.Text('MALAYSIA'),
              ),
              pw.Center(
                child: pw.Text('Negeri ..................'),
              ),
              pw.Center(
                child: pw.Text('Daerah ..................'),
              ),
              pw.SizedBox(height: 10.0),
              pw.Center(
                child: pw.Text('AKTA HARTA PUSAKA KECIL (PEMBAHAGIAN 1955)'),
              ),
              pw.Center(
                child:
                    pw.Text('GUAMAN PEMBAHAGIAN NO. ............. TAHUN 2...'),
              ),
              pw.SizedBox(height: 10.0),
              pw.Row(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Text('Dalam hal Harta Pusaka '),
                  pw.Text(
                    '${widget.application.namaSimati.toUpperCase() ?? ''} , No. kp : ${widget.application.kadPengenalanSimati.toUpperCase() ?? ''} Tarikh Mati:',
                    style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                ],
              ),
              pw.Row(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Text(
                    '${widget.application.tarikhKematian ?? ''}, Surat Sumpah Kematian',
                    style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                  pw.Text('si mati'),
                ],
              ),
              pw.Row(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Text('Petisyen daripada '),
                  pw.Text(
                    '${widget.application.namaPemohon.toUpperCase() ?? ''} , No. KP : ${widget.application.kadPengenalanPemohon ?? ''}',
                    style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                  pw.Text('(Pempetisyen)'),
                ],
              ),
              pw.RichText(
                text: pw.TextSpan(
                  children: [
                    pw.TextSpan(
                      text: 'yang beralamat di ',
                    ),
                    pw.TextSpan(
                      text:
                          widget.application.alamatPemohon.toUpperCase() ?? '',
                      style: pw.TextStyle(
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              pw.Row(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Text('No. untuk dihubungi '),
                  pw.Text(
                    widget.application.telefon.toUpperCase() ?? '',
                    style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                ],
              ),
              pw.SizedBox(height: 10.0),
              pw.Center(
                child: pw.Text(
                  'PETISYEN DI BAWAH SEKSYEN 8',
                  style: pw.TextStyle(
                    fontWeight: pw.FontWeight.bold,
                  ),
                ),
              ),
              pw.SizedBox(height: 5.0),
              pw.Text(
                  'Saya, pempetisyen yang dinamakan di atas menyatakan seperti yang berikut: '),
              pw.SizedBox(height: 5.0),
              pw.Text(
                  '1.  Si mati telah mati pada ${widget.application.tarikhKematian ?? ''}'),
              pw.SizedBox(height: 5.0),
              pw.Flexible(
                fit: pw.FlexFit.loose,
                child: pw.Text(
                  '2.  Saya ialah orang yang menuntut mempunyai kepentingan dalam harta pusaka tersebut sebagai Benefisiari / Pemiutang / Pembeli / Perbadanan / Pegawai Petempatan / Penghulu / Penggawa bagi Mukim....................................................................(*Sebagai benefisiari, pertalian saya dengan si mati ialah  ..................... )',
                ),
              ),
              pw.SizedBox(height: 15.0),
              pw.Flexible(
                child: pw.Text(
                  '3.	Sepanjang pengetahuan dan kepercayaan saya si mati meninggalkan seorang balu/balu-balu/duda dan waris kadim seperti yang berikut: ',
                ),
              ),
              pw.SizedBox(height: 15.0),
              pw.Table(
                border: pw.TableBorder.all(),
                children: [
                  pw.TableRow(
                    verticalAlignment: pw.TableCellVerticalAlignment.middle,
                    children: [
                      pw.Center(
                        child: pw.Text('Nama'),
                      ),
                      pw.Center(
                        child: pw.Text('No. KP'),
                      ),
                      pw.Center(
                        child: pw.Text('Alamat'),
                      ),
                      pw.Center(
                        child: pw.Text(
                          'Hubungan dengan si mati',
                          textAlign: pw.TextAlign.center,
                        ),
                      )
                    ],
                  ),
                  ...widget.application.benificiary.map(
                    (v) => pw.TableRow(
                      verticalAlignment: pw.TableCellVerticalAlignment.middle,
                      children: [
                        pw.Text(v.namaWaris ?? ''),
                        pw.Text(v.kadPengenalanWaris ?? ''),
                        pw.Text(v.alamatWaris ?? ''),
                        pw.Text(v.hubunganWaris ?? ''),
                      ],
                    ),
                  ),
                ],
              ),
              pw.SizedBox(height: 10.0),
              pw.Flexible(
                child: pw.Text(
                  '4.	Si mati pada tarikh kematiannya memiliki harta yang berikut : ',
                ),
              ),
              pw.SizedBox(height: 10.0),
              pw.Center(
                child: pw.Text('HARTA TAK ALIH'),
              ),
              pw.Table(
                border: pw.TableBorder.all(),
                children: [
                  pw.TableRow(
                    verticalAlignment: pw.TableCellVerticalAlignment.middle,
                    children: [
                      pw.Center(
                        child: pw.Text('No. Hak Milik',
                            textAlign: pw.TextAlign.center),
                      ),
                      pw.Center(
                        child: pw.Text('No. Lot'),
                      ),
                      pw.Center(
                        child: pw.Text(
                          'Alamat Harta',
                          textAlign: pw.TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  ...widget.application.inheritance.map(
                    (v) => pw.TableRow(
                      verticalAlignment: pw.TableCellVerticalAlignment.middle,
                      children: [
                        pw.Text(v.noHakMilik ?? ''),
                        pw.Text(v.noLot ?? ''),
                        pw.Text(v.alamat ?? ''),
                      ],
                    ),
                  ),
                ],
              ),
              pw.SizedBox(height: 10.0),
              pw.Flexible(
                child: pw.Text(
                  '5.	Saya memohon supaya harta pusaka si mati boleh dibahagikan mengikut cara yang diperuntukkan oleh Bahagian II Akta Harta Pusaka Kecil (Pembahagian) 1955. ',
                ),
              ),
              pw.SizedBox(height: 10.0),
              pw.Flexible(
                child: pw.Text(
                  '6.	Sepanjang pengetahuan saya, tiada permohonan yang terdahulu bagi pembahagian harta pusaka si mati telah dibuat oleh mana-mana orang dan saya sesungguhnya membuat pengakuan ini dengan penuh kepercayaan bahawa butir-butir yang diberikan di atas adalah benar dan menurut peruntukan Akta Akuan Berkanun 1960. ',
                ),
              ),
              pw.SizedBox(height: 10.0),
              pw.Flexible(
                child: pw.Text(
                    'Bertarikh di .................  pada.................   hari bulan ................. 20 .... '),
              ),
              pw.SizedBox(height: 10.0),
              pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children: [
                  pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                    children: [
                      pw.Text('Ditandatangani dan dengan sesungguhnya,'),
                      pw.Text('dan sebenarnyadiakui oleh yang tersebut '),
                      pw.Text('namanya di atas ............................'),
                      pw.Text(
                        'di ..................................................',
                      ),
                      pw.Text('dalam Negeri ......................'),
                      pw.Text('bertarikh ......................'),
                      pw.SizedBox(height: 15.0),
                      pw.Text('Di hadapan : '),
                      pw.SizedBox(height: 15.0),
                      pw.Text(
                        '..................................................',
                      ),
                      pw.Text('Majistret/Pesuruhjaya Sumpah')
                    ],
                  ),
                  pw.SizedBox(height: 10.0),
                  pw.Column(
                    children: [
                      pw.Text(
                        '.............................................',
                      ),
                      pw.Text('Tandatangan Pempetisyen'),
                    ],
                  ),
                ],
              ),
            ];
            // Center
          },
        ),
      );
      // final file = File(pdf.save());
      // await file.writeAsBytes(await pdf.save());
      //
      final pdfFile = pdf.save();
      print(pdfFile);
      return pdf.save();
    }

    return Scaffold(
      body: Container(
        child: PdfPreview(
          canChangePageFormat: false,
          pdfFileName: 'Borang A',
          build: (s) => pdfFunction(),
        ),
      ),
    );
  }
}
