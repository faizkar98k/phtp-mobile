import 'package:cool_stepper/cool_stepper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/application/application_controller.dart';
import 'package:phtp_mobile/utils/app_config.dart';
import 'package:phtp_mobile/utils/app_modal.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/view/application/benificiary_details.dart';
import 'package:phtp_mobile/view/application/e_borang_pdf.dart';
import 'package:phtp_mobile/view/application/inheritance_details.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';

class AddApplicationDetails extends StatefulWidget {
  @override
  _AddApplicationDetailsState createState() => _AddApplicationDetailsState();
}

class _AddApplicationDetailsState extends State<AddApplicationDetails> {
  ApplicationController applicationCont = Get.put(ApplicationController());

  onSaveApplication() async {
    await applicationCont.updateApplication();
    await applicationCont.getApplication();

    AppRoute.pushReplacement(
      context,
      Obx(
        () => EBorangPDF(application: applicationCont.application.value),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<CoolStep> steps = [
      CoolStep(
        title: "Maklumat Waris",
        subtitle: "Sila lengkapkan maklumat waris untuk teruskan.",
        content: BenificiaryDetails(),
        validation: () => null,
      ),
      CoolStep(
        title: "Maklumat Harta Tak Alih",
        subtitle:
            "Sila lengkapkan maklumat harta tak alih untuk melengkapkan permohonan anda.",
        content: InheritanceDetails(),
        validation: () => null,
      ),
    ];

    return AppMainBar(
      title: 'Maklumat Permohonan',
      child: CoolStepper(
        onCompleted: () async {
          await applicationCont.getApplication();

          if (applicationCont.application.value.benificiary.isEmpty ||
              applicationCont.application.value.inheritance.isEmpty) {
            await AppModal.showInformation(
              context,
              'Permohonan Tidak Sah',
              'Anda perlu mengisi maklumat waris dan maklumat  harta pusaka',
            );
          } else {
            await AppModal.showConfirmation(
              context,
              'Simpan Maklumat',
              'Adakah anda pasti ingin menyimpan maklumat ini ?',
              onSaveApplication,
            );
          }
        },
        steps: steps,
        config: coolStepperConfig,
      ),
    );
  }
}
