import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/card/app_eborang_card.dart';

class EBorang extends StatefulWidget {
  @override
  _EBorangState createState() => _EBorangState();
}

class _EBorangState extends State<EBorang> {
  void downloadFile(String url, String fileName) async {
    final status = await Permission.storage.request();

    if (status.isGranted) {
      final externalDir = await getExternalStorageDirectory();

      await FlutterDownloader.enqueue(
        url: url,
        savedDir: externalDir.path,
        fileName: fileName,
        showNotification: true,
        openFileFromNotification: true,
      );
    } else {
      print("Permission deined");
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'E-Borang',
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            GestureDetector(
              onTap: () => downloadFile(
                formUrl[0],
                "Borang A",
              ),
              child: AppEBorangCard(
                title: 'Borang A',
                subtitle: 'Borang Permohonan Baru',
              ),
            ),
            SizedBox(
              height: AppSize.spaceX16,
            ),
            GestureDetector(
              onTap: () => downloadFile(
                formUrl[1],
                "Borang P",
              ),
              child: AppEBorangCard(
                title: 'Borang P',
                subtitle: 'Borang Permohonan Berikutnya',
              ),
            ),
            SizedBox(
              height: AppSize.spaceX16,
            ),
            GestureDetector(
              onTap: () => downloadFile(
                formUrl[2],
                "Borang AA",
              ),
              child: AppEBorangCard(
                title: 'Borang AA',
                subtitle: 'Borang Penggantian Pemohon',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
