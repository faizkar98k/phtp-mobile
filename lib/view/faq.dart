import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_constant.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';

class FAQ extends StatefulWidget {
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'FAQ',
      child: Container(
        child: ListView.builder(
          itemCount: faq.length,
          itemBuilder: (context, index) {
            return ExpansionTile(
              backgroundColor: AppColors.lightColor,
              expandedAlignment: Alignment.topLeft,
              title: Text(
                faq[index]['question'] ?? '',
                style: TextStyle(
                  color: AppColors.darkBlueColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                ),
              ),
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  margin: EdgeInsets.only(bottom: 16),
                  child: Text(
                    faq[index]['answer'] ?? '',
                    style: TextStyle(
                      color: AppColors.darkTextColor,
                      fontSize: 12,
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
