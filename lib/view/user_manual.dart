import 'package:flutter/material.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class UserManual extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: "Manual Pengguna",
      child: SfPdfViewer.asset('assets/pdf/UserManual.pdf'),
    );
  }
}
