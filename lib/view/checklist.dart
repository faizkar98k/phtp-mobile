import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/checklist_controller.dart';
import 'package:phtp_mobile/data/model/application.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_checklist_text.dart';
import 'package:phtp_mobile/view/share_widget/app_divider.dart';

class CheckList extends StatefulWidget {
  @override
  _CheckListState createState() => _CheckListState();

  final Application application;
  CheckList({this.application});
}

class _CheckListState extends State<CheckList> {
  final checklistCont = Get.put(ChecklistController());

  onTap(int id) async {
    await checklistCont.updateCheckList(id, widget.application.id);
  }

  @override
  void initState() {
    checklistCont.getCheckList(widget.application.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Senarai Semak',
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Permohonan #${widget.application.referenceId}',
                style: TextStyle(
                  fontSize: 24,
                ),
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              Obx(
                () => ListView.separated(
                  separatorBuilder: (context, index) => AppDivider(),
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: checklistCont.checkList.length,
                  itemBuilder: (context, index) {
                    return AppCheckList(
                      index: index + 1,
                      onTap: () => onTap(checklistCont.checkList[index].id),
                      title: checklistCont.checkList[index].title,
                      isCompleted:
                          !(checklistCont.checkList[index].status == 0),
                      // onTap: () => onTap(checklistCont.isCompleted1),
                    );
                  },
                ),
              ),
              SizedBox(
                height: AppSize.spaceX16,
              ),
              Text(
                '** Borang yang disenaraikan diatas perlu dilengkapkan dan dibawa semasa proses pembicaraan **',
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.labelTextColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
