import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  AccountController accountController = Get.put(AccountController());

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Kata Laluan',
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(
            left: AppSize.spaceX16,
            right: AppSize.spaceX16,
            top: AppSize.spaceX24,
          ),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                AppTextFieldWithLabel(
                  controller: oldPasswordController,
                  isChangePassword: false,
                  isPassword: true,
                  label: 'Kata Laluan Lama',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Kata laluan lama tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: newPasswordController,
                  isChangePassword: false,
                  isPassword: true,
                  label: 'Kata Laluan Baru',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Kata laluan baru tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX24,
                ),
                AppButton(
                  onTap: () async {
                    if (formKey.currentState.validate()) {
                      await accountController.changePassword(
                        context,
                        oldPasswordController.text,
                        newPasswordController.text,
                      );
                    }
                  },
                  btnTitle: 'Kemaskini',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
