import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/auth/login.dart';
import 'package:phtp_mobile/view/profile/change_password.dart';
import 'package:phtp_mobile/view/profile/edit_profile.dart';
import 'package:phtp_mobile/view/share_widget/app_list_item.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  AccountController accountController = Get.put(AccountController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: AppSize.spaceX24,
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: AppSize.spaceX16,
            ),
            child: Row(
              children: [
                CircleAvatar(
                  radius: 32,
                  backgroundColor: AppColors.lightColor,
                  child: Text(
                    'User \n Avatar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: AppSize.fontXsSmall,
                      color: AppColors.grayColor,
                    ),
                  ),
                ),
                SizedBox(
                  width: AppSize.spaceX16,
                ),
                Obx(
                  () => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        accountController.user.value.name,
                        style: TextStyle(
                          color: AppColors.labelTextColor,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        accountController.user.value.email,
                        style: TextStyle(
                          color: AppColors.grayColor,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: AppSize.spaceX48,
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: AppSize.spaceX16,
            ),
            child: Text(
              'TETAPAN',
              style: TextStyle(
                color: AppColors.grayColor,
                fontSize: AppSize.fontXsSmall,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          AppListItem(
            onTap: () => AppRoute.push(context, EditProfile()),
            icon: Icon(
              EvaIcons.edit2Outline,
              color: AppColors.labelTextColor,
            ),
            title: 'Kemaskini Profil',
          ),
          AppListItem(
            onTap: () => AppRoute.push(context, ChangePassword()),
            icon: Icon(
              EvaIcons.lockOutline,
              color: AppColors.labelTextColor,
            ),
            title: 'Tukar Kata Laluan',
          ),
          SizedBox(
            height: AppSize.spaceX24,
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: AppSize.spaceX16,
            ),
            child: Text(
              'LAIN-LAIN',
              style: TextStyle(
                color: AppColors.grayColor,
                fontSize: AppSize.fontXsSmall,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          AppListItem(
            onTap: () async {
              await accountController.logout();
              AppRoute.pushAndRemoveUntil(context, Login());
            },
            icon: Icon(
              EvaIcons.logOut,
              color: AppColors.dangerColor,
            ),
            title: 'Log Keluar',
            color: AppColors.dangerColor,
          ),
        ],
      ),
    );
  }
}
