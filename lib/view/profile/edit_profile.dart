import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AccountController accountController = Get.put(AccountController());

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      title: 'Kemaskini Profil',
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(
            left: AppSize.spaceX16,
            right: AppSize.spaceX16,
            top: AppSize.spaceX24,
          ),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                AppTextFieldWithLabel(
                  controller: nameController
                    ..text = accountController.user.value.name,
                  label: 'Nama Penuh',
                  validatorFunction: (val) =>
                      val.isEmpty ? 'Nama penuh tidak boleh kosong' : null,
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: emailController
                    ..text = accountController.user.value.email,
                  enabled: false,
                  label: 'Emel',
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: phoneController
                    ..text = accountController.user.value.profile.phoneNo,
                  label: 'No. Telefon',
                  validatorFunction: (val) =>
                      val.isEmpty ? 'No. Telefon tidak boleh kosong' : null,
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: addressController
                    ..text = accountController.user.value.profile.address,
                  label: 'Alamat',
                  validatorFunction: (val) =>
                      val.isEmpty ? 'Alamat tidak boleh kosong' : null,
                ),
                SizedBox(
                  height: AppSize.spaceX24,
                ),
                AppButton(
                  onTap: () async {
                    if (formKey.currentState.validate()) {
                      await accountController.updateProfile(
                        nameController.text,
                        addressController.text,
                        phoneController.text,
                        context,
                      );
                      // AppRoute.pop(context);
                    }
                  },
                  btnTitle: 'Kemaskini',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
