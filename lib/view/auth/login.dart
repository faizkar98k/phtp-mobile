import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/account_controller.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/auth/register.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/app_loading.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AccountController accountController = Get.put(AccountController());

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      hasAppBar: false,
      child: Obx(
        () => accountController.loginLoading.value
            ? AppLoading()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(AppSize.spaceX16),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Log Masuk',
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: AppSize.fontLargeX3,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: AppSize.spaceX48,
                        ),
                        AppTextFieldWithLabel(
                          controller: accountController.emailController.value,
                          label: 'Emel',
                          validatorFunction: (value) {
                            if (value.isEmpty) {
                              return 'Email field cannot be empty';
                            } else if (!value.isEmail) {
                              return 'Email is in invalid format';
                            }
                          },
                        ),
                        SizedBox(
                          height: AppSize.spaceX16,
                        ),
                        AppTextFieldWithLabel(
                          controller:
                              accountController.passwordController.value,
                          isPassword: true,
                          label: 'Kata Laluan',
                          validatorFunction: (value) {
                            if (value.isEmpty) {
                              return 'Password field cannot be empty';
                            }
                          },
                        ),
                        SizedBox(
                          height: AppSize.spaceX24,
                        ),
                        AppButton(
                          onTap: () async {
                            if (formKey.currentState.validate()) {
                              await accountController.login(context);
                            }
                          },
                          btnTitle: 'Log Masuk',
                        ),
                        SizedBox(
                          height: AppSize.spaceX24,
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () => AppRoute.push(context, Register()),
                            child: Text(
                              'Daftar akaun baru',
                              style: TextStyle(
                                color: AppColors.primaryColor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
