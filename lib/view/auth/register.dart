import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phtp_mobile/controller/register_controller.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:phtp_mobile/view/share_widget/app_bar/app_main_bar.dart';
import 'package:phtp_mobile/view/share_widget/app_button.dart';
import 'package:phtp_mobile/view/share_widget/input/app_text_field_with_label.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RegisterController registerController = Get.put(RegisterController());

  @override
  Widget build(BuildContext context) {
    return AppMainBar(
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(AppSize.spaceX16),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Daftar Akaun',
                  style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: AppSize.fontLargeX3,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: AppSize.spaceX48,
                ),
                AppTextFieldWithLabel(
                  controller: registerController.nameController.value,
                  label: 'Nama Penuh',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Nama penuh tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: registerController.emailController.value,
                  label: 'Emel',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Emel tidak boleh kosong';
                    } else if (!value.isEmail) {
                      return 'Emel salah format';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: registerController.phoneController.value,
                  label: 'No Telefon',
                  keyboardType: TextInputType.number,
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Nombor telefon tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: registerController.addressController.value,
                  label: 'Alamat',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Alamat tidak boleh kosong';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                AppTextFieldWithLabel(
                  controller: registerController.passwordController.value,
                  isPassword: true,
                  label: 'Kata Laluan',
                  validatorFunction: (value) {
                    if (value.isEmpty) {
                      return 'Kata laluan tidak boleh kosong';
                    } else if (value.length < 6) {
                      return 'Kata laluan hendaklah lebih daripada 6 aksara';
                    }
                  },
                ),
                SizedBox(
                  height: AppSize.spaceX24,
                ),
                AppButton(
                  onTap: () async {
                    if (formKey.currentState.validate()) {
                      await registerController.register(context);
                    }
                  },
                  btnTitle: 'Daftar Akaun',
                ),
                SizedBox(
                  height: AppSize.spaceX32,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
