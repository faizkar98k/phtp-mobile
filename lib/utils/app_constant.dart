import 'package:flutter/material.dart';
import 'package:phtp_mobile/view/application/application.dart';
import 'package:phtp_mobile/view/application/applications.dart';
import 'package:phtp_mobile/view/application/create_application.dart';
import 'package:phtp_mobile/view/booking/create_booking.dart';
import 'package:phtp_mobile/view/booking/bookings.dart';
import 'package:phtp_mobile/view/meeting.dart';
import 'package:phtp_mobile/view/profile/profile.dart';

List<Widget> page = <Widget>[
  Application(),
  Meeting(),
  Profile(),
];

const List<String> pageTitle = ['MyPermohonan', 'Meeting', 'Profil'];

List<Map<String, dynamic>> bookingCard = [
  {
    'title': 'Temu Janji Baru',
    'subtitle': 'Mohon temu janji anda dengan pegawai tadbir tanah.',
    'page': CreateBooking(),
  },
  {
    'title': 'Senarai Temu Janji',
    'subtitle': 'Semak temu janji anda disini.',
    'page': Bookings(),
  }
];
List<Map<String, dynamic>> applicationCard = [
  {
    'title': 'Permohonan Baru',
    'subtitle': 'Buat permohonan baru anda disini.',
    'page': CreateApplication(),
  },
  {
    'title': 'Draf Permohonan',
    'subtitle': 'Isi maklumat waris dan harta tak alih disini.',
    'page': Applications(),
  }
];

List<String> formUrl = [
  'https://firebasestorage.googleapis.com/v0/b/phtp-33931.appspot.com/o/Borang%20Permohonan%20Baru%20-%20Bahasa%20Melayu%20Borang%20A%20-%20Seksyen%208.pdf?alt=media&token=03e7ab2c-f6e2-46f3-ab39-59e6ab816730',
  'https://firebasestorage.googleapis.com/v0/b/phtp-33931.appspot.com/o/Borang%20Permohonan%20Berikutnya%20-%20Bahasa%20Melayu%20Borang%20P%20-%20Seksyen%2017.pdf?alt=media&token=740d77f9-ef0e-4dc7-a534-f92d043215b4',
  'https://firebasestorage.googleapis.com/v0/b/phtp-33931.appspot.com/o/Borang%20Penggantian%20Pemohon%20-%20Bahasa%20Melayu%20Borang%20AA.pdf?alt=media&token=455599d7-c230-4425-8968-31f21d7b7807',
];

List<Map<String, dynamic>> faq = [
  {
    'question': 'Apakah yang dimaksudkan dengan harta pusaka?',
    'answer':
        'Harta pusaka ialah harta peninggalan si mati yang mengandungi harta alih dan harta tak alih. Harta seperti wang tunai, syer, caruman KWSP, wang imbuhan insurans, kenderaan, perabot rumah, pakaian dan sebagainya dipanggil harta alih.',
  },
  {
    'question': 'Apakah yang dimaksudkan dengan “harta pusaka kecil”?',
    'answer':
        'Harta peninggalan si mati tanpa wasiat yang bernilai RM 2,000,000.00 ke bawah. Harta yang berikut adalah dianggap sebagai harta pusaka kecil',
  },
  {
    'question': 'Apakah yang dimaksudkan dengan “harta pusaka biasa”?',
    'answer':
        'Harta pusaka biasa bermaksud semua jenis harta peninggalan si mati dengan wasiat atau tanpa wasiat, atau sebahagiannya tanpa wasiat',
  },
  {
    'question':
        'Apakah yang dimaksudkan dengan harta pusaka berwasiat dan/atau tidak berwasiat (testate and intestate estate)?',
    'answer':
        'Jika si mati meninggalkan harta dengan surat wasiat terakhir yang sah yang mengandungi cara-cara bagaimana harta pusakanya hendaklah dibahagikan antara waris- warisnya, harta pusaka itu dipanggil harta pusaka berwasiat.',
  },
  {
    'question': 'Apakah yang dimaksudkan dengan “probet”?',
    'answer':
        'Probet ialah tindakan untuk memperakukan dan mengesahkan sesuatu wasiat yang ditinggalkan oleh si mati. Tujuan probet adalah untuk memberi pengiktirafan kepada sesuatu wasiat yang sah di sisi undang-undang.',
  }
];

List<String> relative = [
  "Tiada Maklumat",
  "Isteri",
  "Suami",
  "Anak Lelaki",
  "Anak Perempuan",
  "Ibu",
  "Bapa",
  "Cucu Lelaki",
  "Cucu Perempuan Dari Anak Lelaki",
  "Saudara Lelaki Sebapa",
  "Saudara Lelaki Seibu",
  "Saudara Lelaki Seibu Sebapa",
  "Saudara Perempuan Sebapa",
  "Saudara Perempuan Seibu",
  "Saudara Perempuan Seibu Sebapa",
  "Anak Lelaki Bapa Saudara Sebapa",
  "Anak Lelaki Bapa Saudara Seibu Sebapa",
  "Anak Saudara Lelaki Sebapa",
  "Anak Saudara Lelaki Seibu Sebapa",
];

List<String> checkList = [
  'Borang permohonan waris si mati',
  'Salinan sijil kematian',
  'Salinan kad pengenalan bagi setiap waris yang berhak',
  '2 salinan CARIAN (RASMI/PERSENDIRIAN) geran versi terkini (RM 75.00) + pelan',
  '1 salinan buku akaun bank/Penyata KWSP/ASN dan lain-lain sekiranya ada',
];

List<String> taraf = [
  'Waris-Waris Simati',
  'Pemiutang dan Pengkaveat',
  // 'Pembeli dibawah surat perjanjian jual beli yang sah',
  // 'Pemegang gadaian atau pemegang pajakan tanah simati',
  // 'Penghulu atau pegawai petempatan yang diarahkan oleh pentadbir tanah',
  'Amanah raya berhad',
  'Penjaga',
  'Baitulmal',
  'Penerima hibah',
  'Pemegang Amanah',
  'Penerima harta sepencarian',
  'Pemegang surat kuasa',
  'Lain-lain orang kepentingan',
  'Saksi yang hadir',
  'Penyewa',
  'Ketua pengarah insolvensi',
  'Pihak berkuasa negeri(PBN)',
  'Kumpulan yang disatukan',
  'Majlis Agama Islam Negeri',
];
