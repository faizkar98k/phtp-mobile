import 'package:flutter/material.dart';
import 'package:cool_stepper/cool_stepper.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

const coolStepperConfig = CoolStepperConfig(
  backText: "Kembali",
  finalText: 'Simpan',
  nextText: "Seterusnya",
  headerColor: AppColors.lightColor,
  iconColor: Colors.transparent,
  titleTextStyle: TextStyle(
    color: AppColors.darkTextColor,
    fontWeight: FontWeight.bold,
  ),
  subtitleTextStyle: TextStyle(
    color: AppColors.grayColor,
    fontSize: 14,
  ),
  stepText: 'Page',
  ofText: 'of',
);
