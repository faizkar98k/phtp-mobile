import 'package:intl/intl.dart';

class AppParseDate {
  static getDateTime(String dateTime) {
    DateFormat dateFormat = DateFormat("dd-MM-yyyy - hh:mm");
    return dateFormat.format(DateTime.parse(dateTime));
  }
}
