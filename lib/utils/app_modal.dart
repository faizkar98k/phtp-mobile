import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/app_route.dart';
import 'package:phtp_mobile/utils/app_size.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';

class AppModal {
  static showConfirmation(
    BuildContext context,
    title,
    subtitle,
    onTapFunction,
  ) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Container(
            height: 175.0,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(
                    vertical: AppSize.spaceX16,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: AppColors.lightColor,
                      ),
                    ),
                  ),
                  child: Text(
                    title ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(
                  height: AppSize.spaceX8,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: AppSize.spaceX16,
                  ),
                  child: Text(
                    subtitle ?? '',
                    style: TextStyle(
                      color: AppColors.labelTextColor,
                      fontSize: AppSize.fontMedium,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        AppRoute.pop(context);
                      },
                      child: Container(
                        height: 52,
                        width: 120,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: AppColors.lightColor,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: Text(
                            'TIDAK',
                            style: TextStyle(
                              color: AppColors.labelTextColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        AppRoute.pop(context);
                        onTapFunction();
                      },
                      child: Container(
                        height: 52,
                        width: 120,
                        decoration: BoxDecoration(
                          color: AppColors.primaryColor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: Text(
                            'YA',
                            style: TextStyle(
                              color: AppColors.whiteColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  static showInformation(BuildContext context, title, subtitle, {onTap}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Container(
            padding: EdgeInsets.only(bottom: 16),
            width: 375.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 375.0,
                  padding: EdgeInsets.symmetric(
                    vertical: AppSize.spaceX16,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: AppColors.lightColor,
                      ),
                    ),
                  ),
                  child: Text(
                    title ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(
                  height: AppSize.spaceX8,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: AppSize.spaceX16,
                  ),
                  child: Text(
                    subtitle ?? '',
                    style: TextStyle(
                      color: AppColors.labelTextColor,
                      fontSize: AppSize.fontMedium,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: AppSize.spaceX16,
                ),
                GestureDetector(
                  onTap: () {
                    AppRoute.pop(context);
                    onTap();
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    height: 52,
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Center(
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: AppColors.whiteColor,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
