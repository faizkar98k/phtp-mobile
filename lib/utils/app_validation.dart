class AppValidation {
  static emptyValidation(String value, String fieldName) {
    if (value.isEmpty) {
      return '$fieldName cannot be empty';
    } else {
      return '';
    }
  }
}
