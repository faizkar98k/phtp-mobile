import 'package:flutter/material.dart';
import 'package:phtp_mobile/utils/style/app_colors.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AppRefresher extends StatelessWidget {
  final RefreshController controller;
  final bool enablePullDown;
  final bool enablePullUp;
  final Function onRefresh;
  final Function onLoading;
  final Widget child;
  final ScrollController scrollController;
  final Color textColor;

  AppRefresher({
    this.controller,
    this.enablePullDown,
    this.enablePullUp,
    this.onRefresh,
    this.onLoading,
    this.child,
    this.scrollController,
    this.textColor = Colors.black,
  });

  @override
  Widget build(BuildContext context) {
    return RefreshConfiguration(
      headerTriggerDistance: 60.0,
      child: SmartRefresher(
        controller: controller,
        child: child,
        enablePullDown: enablePullDown,
        enablePullUp: enablePullUp,
        onRefresh: onRefresh,
        scrollController: scrollController,
        onLoading: onLoading,
        header: ClassicHeader(
          outerBuilder: (child) {
            return Container(
              padding: EdgeInsets.only(
                top: 100,
                bottom: 20,
              ),
              color: Colors.transparent,
              child: child,
            );
          },
          idleText: 'Pull down to refresh',
          refreshingText: 'Refreshing',
          completeText: 'Refresh Complete',
          failedText: 'Unable to load data',
          releaseText: 'Release to refresh',
          refreshingIcon: loaderWidget(),
          textStyle: TextStyle(
            color: textColor,
          ),
        ),
        footer: ClassicFooter(
          idleText: 'Pull down to refresh',
          loadingText: "Loading",
          noDataText: 'No data Text',
          failedText: 'Unable to load data',
          loadingIcon: loaderWidget(),
        ),
      ),
    );
  }

  Widget loaderWidget() {
    return SizedBox(
      height: 20.0,
      width: 20.0,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.primaryColor),
      ),
    );
  }
}
